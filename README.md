# Requisitos

- [node](https://nodejs.org) >= 18.16
- [Apache Web Server](https://httpd.apache.org/download.cgi)
# Instalação
Clonar o repositório:
```bash
$ git clone https://gitlab.com/TiLopes/pap_frontend
```
Instalar as bibliotecas:
```bash
$ yarn install
```
Alterar `baseURL` da biblioteca axios:
```javascript
// ficheiro em src/helper/axiosInstance.js
const axiosInstance = axios.create({
  baseURL: 'endereço da API',
});
...
```
# Implantação no Apache
Gerar ficheiros JavaScript, HTML e CSS:
```bash
$ yarn run build
```
Copiar ficheiros gerados em `dist/` para a pasta do Apache. Exemplo do ficheiro `vhosts.conf`:
```txt
...
<VirtualHost *:8080>
  ServerName example.com
  DocumentRoot /var/www/httpd/example.com

  <Directory "/var/www/httpd/example.com">
    ...

    RewriteEngine On
    # Don't rewrite files or directories
    RewriteCond %{REQUEST_FILENAME} -f [OR]
    RewriteCond %{REQUEST_FILENAME} -d
    RewriteRule ^ - [L]
    # Rewrite everything else to index.html to allow html5 state links
    RewriteRule ^ index.html [L]
  </Directory>
</VirtualHost>
...
```
O website deve estar a funcionar quando aceder ao `localhost`.
