import react from '@vitejs/plugin-react';
import path from 'path';
import UnoCSS from 'unocss/vite';
import presetUno from '@unocss/preset-uno';
import { defineConfig } from 'vite';
import { presetWind, presetTypography } from 'unocss';
import tsconfigPaths from 'vite-tsconfig-paths'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react(), UnoCSS(), tsconfigPaths({
    projects: ["jsconfig.json"]
  })],
  resolve: {
    alias: {
      '@styles': path.resolve(__dirname, './src/styles'),
      '@components': path.resolve(__dirname, './src/components'),
      '@helpers': path.resolve(__dirname, './src/helper'),
      '@context': path.resolve(__dirname, './src/context'),
    },
  },
});
