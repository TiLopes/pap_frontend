import { defineConfig } from 'unocss';
import presetUno from '@unocss/preset-uno';
import { presetWind, presetTypography } from 'unocss'

export default defineConfig({
  presets: [presetUno(), presetWind(), presetTypography()],
});
