import React, { Suspense } from 'react';
import ReactDOM from 'react-dom/client';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import LoadingSpinner from './components/LoadingSpinner';
import Frontpage from './pages/Frontpage';
import { AuthProvider } from './context/Auth.context';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import { RolesAuthRoute } from './guard/RoleGuard';
import { setDefaultOptions } from 'date-fns';
import loadable from '@loadable/component';
import '@unocss/reset/tailwind-compat.css';
import 'virtual:uno.css';
import './index.scss';
import pt from 'date-fns/locale/pt';

setDefaultOptions({ locale: pt });

// página "lazy-loaded"
const LoginCondominio = loadable(() => import('./pages/Auth/Login/Condominio'), {
  fallback: <LoadingSpinner />,
});

const LoginCondomino = loadable(() => import('./pages/Auth/Login/Condomino'), {
  fallback: <LoadingSpinner />,
});
const SignupCondominio = loadable(() => import('./pages/Auth/Signup/Condominio'), {
  fallback: <LoadingSpinner />,
});
const AdminLayout = loadable(() => import('./pages/Admin/Layout'));
const Admin = loadable(() => import('pages/Admin'));
const AdminProfile = loadable(() => import('./pages/Admin/Perfil'), {
  fallback: <LoadingSpinner />,
});
const AdminFracoes = loadable(() => import('./pages/Admin/Fracoes'), {
  fallback: <LoadingSpinner />,
});
const AdminCondomino = loadable(() => import('./pages/Admin/Condominos'), {
  fallback: <LoadingSpinner />,
});
const FracaoCriar = loadable(() => import('pages/Admin/Fracoes/Criar'), {
  fallback: <LoadingSpinner />,
});
const AdminCriarCondomino = loadable(() => import('./pages/Admin/Condominos/Criar'), {
  fallback: <LoadingSpinner />,
});
const AdminOcorrencias = loadable(() => import('./pages/Admin/Ocorrencias'), {
  fallback: <LoadingSpinner />,
});
const AdminCriarOcorrencia = loadable(() => import('./pages/Admin/Ocorrencias/Criar'), {
  fallback: <LoadingSpinner />,
});
const AdminQuotas = loadable(() => import('./pages/Admin/Quotas'), {
  fallback: <LoadingSpinner />,
});
const CriarQuota = loadable(() => import('./pages/Admin/Quotas/Criar'), {
  fallback: <LoadingSpinner />,
});
const AdminMovimentos = loadable(() => import('./pages/Admin/Movimentos'), {
  fallback: <LoadingSpinner />,
});
const CriarMovimento = loadable(() => import('./pages/Admin/Movimentos/Registar'), {
  fallback: <LoadingSpinner />,
});
const AdminPagamentoQuotas = loadable(() => import('./pages/Admin/Quotas/PagamentoQuotas'), {
  fallback: <LoadingSpinner />,
});
const AdminRegistarPagamentoQuota = loadable(
  () => import('./pages/Admin/Quotas/RegistarPagamentoQuota'),
  {
    fallback: <LoadingSpinner />,
  },
);
const AdminRegistarReceita = loadable(() => import('./pages/Admin/Movimentos/Receita'), {
  fallback: <LoadingSpinner />,
});
const AdminRegistarDespesa = loadable(() => import('./pages/Admin/Movimentos/Despesa'), {
  fallback: <LoadingSpinner />,
});
const LayoutCondominos = loadable(() => import('./pages/Condominos/Layout'));
const CondominosFront = loadable(() => import('./pages/Condominos'));
const CondominoPerfil = loadable(() => import('./pages/Condominos/Perfil'), {
  fallback: <LoadingSpinner />,
});
const CondominoOcorrencias = loadable(() => import('./pages/Condominos/Ocorrencias'), {
  fallback: <LoadingSpinner />,
});
const CondominoCriarOcorrencia = loadable(() => import('./pages/Condominos/Ocorrencias/Criar'), {
  fallback: <LoadingSpinner />,
});
const CondominoFracoes = loadable(() => import('./pages/Condominos/Fracoes'), {
  fallback: <LoadingSpinner />,
});
const CondominoQuotas = loadable(() => import('./pages/Condominos/Quotas'), {
  fallback: <LoadingSpinner />,
});
const CondominoPagamentosQuotas = loadable(() => import('./pages/Condominos/Quotas/Pagamentos'), {
  fallback: <LoadingSpinner />,
});

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
    },
  },
});

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <QueryClientProvider client={queryClient}>
      <Router>
        <AuthProvider>
          <Routes>
            <Route path='/'>
              <Route index element={<Frontpage></Frontpage>}></Route>
              <Route
                path='signup/condominio'
                element={
                  <Suspense fallback={<LoadingSpinner />}>
                    <SignupCondominio />
                  </Suspense>
                }
              />
              <Route path='login'>
                <Route index element={<LoginCondomino />} />
                <Route path='condominio' element={<LoginCondominio />} />
              </Route>
              <Route
                path='condominos'
                element={
                  <RolesAuthRoute roles={[1]}>
                    <LayoutCondominos />
                  </RolesAuthRoute>
                }
              >
                <Route index element={<CondominosFront />}></Route>
                <Route path='perfil' element={<CondominoPerfil />}></Route>
                <Route path='ocorrencias'>
                  <Route index element={<CondominoOcorrencias />}></Route>
                  <Route path='criar' element={<CondominoCriarOcorrencia />}></Route>
                </Route>
                <Route path='fracoes' element={<CondominoFracoes />}></Route>
                <Route path='quotas'>
                  <Route index element={<CondominoQuotas />}></Route>
                  <Route path='pagamentos' element={<CondominoPagamentosQuotas />}></Route>
                </Route>
              </Route>
              <Route
                path='administracao'
                element={
                  <RolesAuthRoute roles={[999]}>
                    <AdminLayout />
                  </RolesAuthRoute>
                }
              >
                <Route index element={<Admin />}></Route>
                <Route path='perfil' element={<AdminProfile />} />
                <Route path='fracoes' element={<AdminFracoes />}></Route>
                <Route path='fracoes/criar' element={<FracaoCriar />} />
                <Route
                  path='condomino'
                  element={
                    <Suspense fallback={<LoadingSpinner />}>
                      <AdminCondomino />
                    </Suspense>
                  }
                ></Route>
                <Route
                  path='condomino/criar'
                  element={
                    <Suspense fallback={<LoadingSpinner />}>
                      <AdminCriarCondomino />
                    </Suspense>
                  }
                />
                <Route path='ocorrencias'>
                  <Route
                    index
                    element={
                      <Suspense fallback={<LoadingSpinner />}>
                        <AdminOcorrencias />
                      </Suspense>
                    }
                  ></Route>
                  <Route
                    path='criar'
                    element={
                      <Suspense fallback={<LoadingSpinner />}>
                        <AdminCriarOcorrencia />
                      </Suspense>
                    }
                  />
                </Route>
                <Route path='quotas'>
                  <Route
                    index
                    element={
                      <Suspense fallback={<LoadingSpinner />}>
                        <AdminQuotas />
                      </Suspense>
                    }
                  ></Route>
                  <Route
                    path='criar'
                    element={
                      <Suspense fallback={<LoadingSpinner />}>
                        <CriarQuota />
                      </Suspense>
                    }
                  />
                  <Route path='pagamento'>
                    <Route
                      index
                      element={
                        <Suspense fallback={<LoadingSpinner />}>
                          <AdminPagamentoQuotas />
                        </Suspense>
                      }
                    ></Route>
                    <Route
                      path='registar'
                      element={
                        <Suspense fallback={<LoadingSpinner />}>
                          <AdminRegistarPagamentoQuota />
                        </Suspense>
                      }
                    ></Route>
                  </Route>
                </Route>
                <Route path='movimentos'>
                  <Route
                    index
                    element={
                      <Suspense fallback={<LoadingSpinner />}>
                        <AdminMovimentos />
                      </Suspense>
                    }
                  ></Route>
                  <Route
                    path='registar'
                    element={
                      <Suspense fallback={<LoadingSpinner />}>
                        <CriarMovimento />
                      </Suspense>
                    }
                  >
                    <Route
                      path='receita'
                      element={
                        <Suspense fallback={<LoadingSpinner />}>
                          <AdminRegistarReceita />
                        </Suspense>
                      }
                    ></Route>
                    <Route
                      path='despesa'
                      element={
                        <Suspense fallback={<LoadingSpinner />}>
                          <AdminRegistarDespesa />
                        </Suspense>
                      }
                    ></Route>
                  </Route>
                </Route>
              </Route>
            </Route>
          </Routes>
        </AuthProvider>
      </Router>
    </QueryClientProvider>
  </React.StrictMode>,
);
