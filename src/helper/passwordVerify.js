import { useState, useEffect } from 'react';

/**
 * Hook personalizado para validar uma senha.
 * @param {Object} options - Opções de configuração.
 * @param {string} options.password - A senha a ser validada.
 * @param {number} options.requiredLength - O comprimento mínimo exigido da senha.
 * @returns {Array.<boolean>} Um array contendo os resultados da validação.
 *   O primeiro elemento é um booleano indicando se a senha possui o comprimento válido.
 *   O segundo elemento é um booleano indicando se a senha contém um número.
 *   O terceiro elemento é um booleano indicando se a senha contém uma letra maiúscula.
 *   O quarto elemento é um booleano indicando se a senha contém uma letra minúscula.
 *   O quinto elemento é um booleano indicando se a senha contém um caractere especial.
 */
export const usePasswordValidation = ({ password = '', requiredLength = 8 }) => {
  /**
   * Estado que indica se a senha possui o comprimento válido.
   * @type {[boolean, function]}
   */
  const [validLength, setValidLength] = useState(null);

  /**
   * Estado que indica se a senha contém um número.
   * @type {[boolean, function]}
   */
  const [hasNumber, setHasNumber] = useState(null);

  /**
   * Estado que indica se a senha contém uma letra maiúscula.
   * @type {[boolean, function]}
   */
  const [upperCase, setUpperCase] = useState(null);

  /**
   * Estado que indica se a senha contém uma letra minúscula.
   * @type {[boolean, function]}
   */
  const [lowerCase, setLowerCase] = useState(null);

  /**
   * Estado que indica se a senha contém um caractere especial.
   * @type {[boolean, function]}
   */
  const [specialChar, setSpecialChar] = useState(null);

  useEffect(() => {
    setValidLength(password.length >= requiredLength);
    setUpperCase(password.toLowerCase() !== password);
    setLowerCase(password.toUpperCase() !== password);
    setHasNumber(/\d/.test(password));
    setSpecialChar(/[ `!@#$%^&*()_+\-=\]{};':"\\|,.<>?~]/.test(password));
  }, [password]);

  return [validLength, hasNumber, upperCase, lowerCase, specialChar];
};
