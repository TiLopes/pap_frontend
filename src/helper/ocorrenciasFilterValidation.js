import * as Yup from 'yup';

export const ocorrenciasFilterValidation = Yup.object({
  data_inicial: Yup.date('Data inválida')
    .nullable()
    .transform((curr, orig) => (orig === '' ? null : curr))
    .max(new Date(), 'Escolha uma data até presente')
    .test('', 'Data inválida', function (value) {
      const data_final = this.parent.data_final ? this.parent.data_final.getTime() : null;

      if (data_final == null && value == null) {
        return true;
      }

      if (data_final && value) {
        if (value.getTime() > data_final) {
          return false;
        }
      } else {
        return false;
      }
      return true;
    }),
  data_final: Yup.date('Data inválida')
    .nullable()
    .transform((curr, orig) => (orig === '' ? null : curr))
    .max(new Date(), 'Escolha uma data até presente')
    .test('', 'Data inválida', function (value) {
      const data_inicial = this.parent.data_inicial ? this.parent.data_inicial.getTime() : null;

      if (data_inicial == null && value == null) {
        return true;
      }

      if (data_inicial && value) {
        if (value.getTime() < data_inicial) {
          return false;
        }
      } else {
        return false;
      }

      return true;
    }),
  estado: Yup.object().nullable(),
});
