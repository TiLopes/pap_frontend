import { createContext, useContext, useState, useEffect } from 'react';
import axiosInstance from '../helper/axiosInstance';
import { useNavigate } from 'react-router-dom';

/**
 * @typedef {Object} AuthContextType
 * @property {function(string, string): void} loginCondominio - Função de login para utilizador do tipo "condomínio".
 * @property {function(string, string): void} loginCondomino - Função de login para utilizador do tipo "condómino".
 * @property {function(string, string, string, string, string, string): Promise<any>} signup - Função de registo.
 * @property {function(string): Promise<any>} resetPasswordRequest - Função de pedido de reset de password.
 * @property {function(string, string): Promise<any>} resetPassword - Função de reset de password.
 * @property {function(): void} logout - Função de logout.
 * @property {function(): boolean} isLoggedIn - Verifica se o utilizador está autenticado.
 * @property {function(): boolean} isLoggedOut - Verifica se o utilizador não está autenticado.
 * @property {function(): Date|null} getExpiration - Obtém a data de expiração da sessão do utilizador.
 */
const AuthContext = createContext(null);

/**
 * Hook personalizado para aceder ao contexto de autenticação.
 * @returns {AuthContextType} O contexto de autenticação.
 */
export const useAuth = () => useContext(AuthContext);

/**
 * Componente provedor de autenticação.
 * @param {Object} props - Propriedades do componente.
 * @param {React.ReactNode} props.children - Componentes filhos.
 */
export const AuthProvider = ({ children }) => {
  const [authResult, setAuthResult] = useState(null);
  const navigate = useNavigate();

  useEffect(() => {
    // se ocorrer um erro não redirecionar
    if (!authResult) {
      return;
    }

    // criar sessão
    setSession(authResult);

    // redirecionar para outra página dependendo do tipo de utilizador
    switch (authResult.id_grupo) {
      case 1:
        navigate('/condominos');
        break;
      case 999:
        navigate('/administracao/dashboard');
        break;

      default:
        break;
    }
  }, [authResult]);

  const loginCondominio = (email, password) => {
    axiosInstance
      .post('/api/auth/login', {
        email,
        password,
        userType: 'condominio',
      })
      .then((res) => {
        setAuthResult(res.data);
      });
  };

  const loginCondomino = (username, password) => {
    axiosInstance
      .post('/api/auth/login', {
        username,
        password,
        userType: 'condomino',
      })
      .then((res) => setAuthResult(res.data));
  };

  const signup = (email_admin, password, nome, morada, nif, cod_postal) => {
    return axiosInstance.post('/api/auth/signup', {
      email_admin,
      password,
      nome,
      nif,
      morada,
      cod_postal,
    });
  };

  const resetPasswordRequest = (email) => {
    return axiosInstance.post(
      '/api/reset/password/request',
      {
        email,
      },
      {
        responseType: 'text',
      },
    );
  };

  const resetPassword = (password, token) => {
    return axiosInstance
      .post('/api/reset/password', {
        password,
        token,
      })
      .catch((err) => {
        console.log(err);
        // this.router.navigateByUrl('/');
        throw new Error('ups something happened');
      });
  };

	// criar sessão (guarda informação na localStorage do browser)
  const setSession = (authResult) => {
    const expiresAt = new Date(authResult.accessToken.expires * 1000);

    localStorage.setItem('token', authResult.accessToken.token);
    localStorage.setItem('expires_at', JSON.stringify(expiresAt.valueOf()));
    localStorage.setItem('group', authResult.id_grupo);
  };

  const logout = async () => {
    await axiosInstance.post('/api/auth/logout', {
      userType: 'condominio',
    });
    localStorage.removeItem('token');
    localStorage.removeItem('expires_at');
    localStorage.removeItem('group');
    navigate('/');
  };

  const isLoggedIn = () => {
    if (!getExpiration()?.valueOf()) {
      return false;
    }
    return Date.now() < getExpiration().valueOf();
  };

  const isLoggedOut = () => !isLoggedIn();

  const getExpiration = () => {
    const expiration = localStorage.getItem('expires_at');
    if (expiration) {
      return new Date(JSON.parse(expiration));
    }
    return null;
  };

  return (
    <AuthContext.Provider
      value={{
        loginCondominio,
        loginCondomino,
        signup,
        resetPasswordRequest,
        resetPassword,
        logout,
        isLoggedIn,
        isLoggedOut,
        getExpiration,
        setSession,
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};
