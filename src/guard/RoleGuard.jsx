import axiosInstance from '@helpers/axiosInstance';
import { useEffect, useState } from 'react';
import { Navigate, useNavigate } from 'react-router-dom';

export function RolesAuthRoute({ children, roles }) {
  const userRoles = Number(localStorage.getItem('group'));
  const [canAccess, setCanAccess] = useState(false);
  const navigate = useNavigate();

  useEffect(() => {
    axiosInstance
      .post('/api/verify/group', {
        token: localStorage.getItem('token'),
        group: userRoles,
      })
      .then((response) => {
        const hasAccess = roles.includes(userRoles);
        setCanAccess(hasAccess);

        if (!hasAccess) navigate('/');
      })
      .catch((error) => {
        console.log(error);
        setCanAccess(false);
        navigate('/');
      });
  }, []);

  if (canAccess) {
    return <>{children}</>;
  }
}
