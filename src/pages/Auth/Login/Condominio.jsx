import { useForm } from 'react-hook-form';
import '@styles/Login.scss';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';
import { useMutation } from '@tanstack/react-query';
import axiosInstance from '@helpers/axiosInstance';
import { useNavigate } from 'react-router-dom';
import { useAuth } from '@context/Auth.context';

function LoginCondominio() {
  const navigate = useNavigate();
  const { setSession, getExpiration } = useAuth();

  // modelo de verificação
  const validationSchema = Yup.object({
    email: Yup.string().email('Email inválido').required('Campo obrigatório'),
    password: Yup.string().required('Campo obrigatório'),
  });

  const {
    register,
    handleSubmit,
    formState: { errors, isValid },
    clearErrors,
    setError,
  } = useForm({
    mode: 'all',
    resolver: yupResolver(validationSchema),
  });

  // função para fazer o login
  const loginCondominio = useMutation({
    mutationFn: (data) => {
      return axiosInstance.post('/api/auth/login', {
        email: data.email,
        password: data.password,
        userType: 'condominio',
      });
    },
    onSuccess: ({ data }) => {
      setSession(data);
      navigate('/administracao');
    },
    onError: ({ response: { data } }) => {
      console.log(data.errors);
      clearErrors();
      if (data.errors.email)
        setError('email', { type: 'manual', message: data.errors.email }, { shouldFocus: true });
      if (data.errors.password)
        setError(
          'password',
          { type: 'manual', message: data.errors.password },
          { shouldFocus: true },
        );
    },
  });

  // quando o formulário é submetido fazer login se os dados forem válidos
  const onSubmit = (data) => loginCondominio.mutate({ email: data.email, password: data.password });

  return (
    <div className='login flex min-h-screen min-w-full flex-col items-center justify-center'>
      <h1>Entrar em condomínio</h1>

      <form onSubmit={handleSubmit(onSubmit)}>
        <div className='input-wrapper'>
          <label htmlFor='email'>Email</label>
          <input type='email' name='email' {...register('email')} />
          <div className='error-message'>{errors.email?.message}</div>
        </div>
        <div className='input-wrapper'>
          <label htmlFor='password'>Password</label>
          <input type='password' name='password' {...register('password')} />
          <div className='error-message'>{errors.password?.message}</div>
        </div>

        <button
          type='submit'
          className='w-full cursor-pointer rounded bg-[#1d1b31] py-1.5 font-bold text-white disabled:cursor-auto'
          disabled={!isValid}
        >
          Entrar
        </button>
      </form>
    </div>
  );
}

export default LoginCondominio;
