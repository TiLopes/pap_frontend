import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';
import '@styles/Login.scss';
import { useAuth } from '@context/Auth.context';
import axiosInstance from '@helpers/axiosInstance';
import { useMutation } from '@tanstack/react-query';
import { useNavigate } from 'react-router-dom';

function LoginCondomino() {
  const { setSession } = useAuth();
  const navigate = useNavigate();

  // modelo de verificação
  const validationSchema = Yup.object({
    username: Yup.string().required('Campo obrigatório'),
    password: Yup.string().required('Campo obrigatório'),
  });

  const {
    register,
    handleSubmit,
    clearErrors,
    setError,
    formState: { errors },
  } = useForm({
    mode: 'all',
    resolver: yupResolver(validationSchema),
  });

  // função para fazer o login
  const loginCondomino = useMutation({
    mutationFn: ({ username, password }) => {
      return axiosInstance.post('/api/auth/login', {
        username,
        password,
        userType: 'condomino',
      });
    },
    onSuccess: ({ data }) => {
      setSession(data);
      navigate('/condominos');
    },
    onError: ({ response: { data } }) => {
      console.log(data.errors);
      clearErrors();
      if (data.errors.username)
        setError(
          'username',
          { type: 'manual', message: data.errors.username },
          { shouldFocus: true },
        );
      if (data.errors.password)
        setError(
          'password',
          { type: 'manual', message: data.errors.password },
          { shouldFocus: true },
        );
    },
  });

  // fazer login se os dados forem válidos
  const onSubmit = (data) =>
    loginCondomino.mutate({
      username: data.username,
      password: data.password,
    });

  return (
    <div className='login flex min-h-screen min-w-full flex-col items-center justify-center'>
      <h1>Entrar como condómino</h1>

      {/* Associar o form através do evento onSubmit */}
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className='input-wrapper'>
          <label>Nome de utilizador</label>
          {/* register(...) associa um valor ao formulário */}
          <input type='text' {...register('username')} />
          <div className='error-message'>{errors.username?.message}</div>
        </div>
        <div className='input-wrapper'>
          <label>Password</label>
          <input type='password' {...register('password')} />
          <div className='error-message'>{errors.password?.message}</div>
        </div>
        <button
          type='submit'
          className='w-full cursor-pointer rounded  bg-[#1d1b31] py-1.5
					font-semibold text-white disabled:cursor-auto'
        >
          Entrar
        </button>
      </form>
    </div>
  );
}

export default LoginCondomino;
