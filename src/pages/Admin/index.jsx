import axiosInstance from '@helpers/axiosInstance';
import { useQuery } from '@tanstack/react-query';
import { useNavigate } from 'react-router-dom';

const index = () => {
  const navigate = useNavigate();
  const condominioInfo = useQuery({
    queryKey: ['administracao'],
    queryFn: () => {
      return axiosInstance.get('/api/condominio/info');
    },
    onError: () => {
      navigate('/');
    },
  });

  if (condominioInfo.isSuccess)
    return (
      <section className={'mx-[clamp(16px,3svw,32px)]'}>
        <h1 className={'prose'}>Bem vindo {condominioInfo.data.data.condominio.nome}!</h1>
        <p className={'prose'}>
          Utilize a barra de navegação no lado esquerdo para navegar na nossa plataforma.
        </p>
      </section>
    );
};

export default index;
