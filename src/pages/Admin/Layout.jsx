import { Outlet, useNavigate } from 'react-router-dom';
import AdminNavbar from '@components/AdminNavbar';
import { Helmet, HelmetProvider } from 'react-helmet-async';
import { useAuth } from '@context/Auth.context';
import { SnackbarProvider } from 'notistack';
import '@styles/AdminCondominio.scss';

function Admin() {
	const navigate = useNavigate();

	const { isLoggedOut } = useAuth();

	if (isLoggedOut()) {
		navigate('/login/condominio');
		return;
	}

	return (
		<HelmetProvider>
			<SnackbarProvider
				maxSnack={3}
				anchorOrigin={{
					horizontal: 'right',
					vertical: 'top',
				}}
			>
				<Helmet>
					<title>Administração condomínio</title>
					<link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet' />
				</Helmet>
				<AdminNavbar />
				<section className='home-section h-full' id='home-section'>
					<div className='home-content h-full w-full'>
						<Outlet />
					</div>
				</section>
			</SnackbarProvider>
		</HelmetProvider>
	);
}

export default Admin;
