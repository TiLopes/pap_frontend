import { Controller, useForm } from 'react-hook-form';
import '@styles/AdminFracoes.scss';
import { useQuery, useMutation } from '@tanstack/react-query';
import Select from 'react-select';
import axiosInstance from '@helpers/axiosInstance';
import { useSnackbar } from 'notistack';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';
import { useState } from 'react';
import { Link } from 'react-router-dom';

function FracaoCriar() {
  const { enqueueSnackbar } = useSnackbar();

  const criarFracao = useMutation({
    mutationFn: async (data) => {
      // fazer pedido post à API com os dados do formulário
      await axiosInstance.post('/api/create/fracao', {
        fracao: data.fracao,
        andar: data.andar,
        escritura: data.escritura,
        permilagem_rateio: data.permilagem_rateio,
        tipoFracao: data.tipoFracao.value,
      });
    },
    onSuccess: () => {
      // mostrar notificação de sucesso
      enqueueSnackbar('Criada com sucesso', {
        variant: 'success',
      });
    },
    onError: () => {
      // mostrar notificação de erro
      enqueueSnackbar('Oops. Tente novamente', {
        variant: 'error',
      });
    },
  });

  // opções dos tipos das frações
  const [optionsTipoFracao, setOptionsTipoFracao] = useState([]);

  const tiposFracao = useQuery({
    // fazer pedido ao servidor para obter os tipos das frações
    queryFn: async () => {
      return await axiosInstance.get('/api/tipos/fracoes');
    },
    queryKey: ['fracoes', 'criar'],
    // se for feito com sucesso adicionar os tipos ao array optionsTipoFracao
    onSuccess: (res) => {
      for (const tipo of res.data.tipos) {
        setOptionsTipoFracao((prev) => [...prev, { value: tipo.id, label: tipo.descricao }]);
      }
    },
  });

  // validar todos os dados
  const criarValidacao = Yup.object({
    fracao: Yup.string().required('Preencha este campo'),
    andar: Yup.string()
      .required('Preencha este campo')
      .matches(/^\d+º [A-Z]{0,3}$/, 'Formato inválido'),
    escritura: Yup.string().required('Preencha este campo').matches(/^\d*$/, 'Valor inválido'),
    permilagem_rateio: Yup.string()
      .required('Preencha este campo')
      .matches(/^\d*$/, 'Valor inválido'),
    tipoFracao: Yup.object({}).nullable('').required('Preencha este campo'),
  });

  const {
    register,
    handleSubmit,
    control,
    formState: { errors },
    setValue,
  } = useForm({
    // utilizar o modelo de verificação
    resolver: yupResolver(criarValidacao),
    mode: 'all',
    // definição de valores predefinidos
    defaultValues: {
      escritura: 0,
      fracao: '',
      andar: '',
      permilagem_rateio: 0,
      tipoFracao: optionsTipoFracao[0],
    },
  });

  // submissão do formulário se os dados estiverem válidos
  const onSubmit = async (formData) => {
    criarFracao.mutate(formData);
  };

  return (
    <section className='fracao-criar'>
      <h1 className='prose mb-4'>Adicionar fração</h1>
      <form onSubmit={handleSubmit(onSubmit)}>
        <div className='input-wrapper !w-1/2 pr-4'>
          <label>Fração</label>
          <input type='text' {...register('fracao')} placeholder='Ex: A' />
          <div className='error-message w-full'>{errors.fracao?.message}</div>
        </div>
        <div className='input-wrapper !w-1/2 pl-4'>
          <label>Andar</label>
          <input type='text' {...register('andar')} placeholder='Ex: 1º E' />
          <div className='error-message w-full'>{errors.andar?.message}</div>
        </div>
        <div className='input-wrapper !w-1/2 pr-4'>
          <label>Permilagem escritura</label>
          <input
            type='text'
            {...register('escritura', { valueAsNumber: true })}
            onChange={(e) => setValue('permilagem_rateio', e.target.value)}
            className=''
          />
          <div className='error-message w-full'>{errors.escritura?.message}</div>
        </div>
        <div className='input-wrapper !w-1/2 pl-4'>
          <label>Permilagem rateio</label>
          <input type='text' {...register('permilagem_rateio')} />
          <div className='error-message w-full'>{errors.permilagem_rateio?.message}</div>
        </div>
        <div className='input-wrapper'>
          <label>Tipo da fração</label>
          <Controller
            render={({ field }) => (
              <Select
                {...field}
                options={optionsTipoFracao}
                className='w-full'
                placeholder='Indique o tipo da fração'
              />
            )}
            name='tipoFracao'
            control={control}
          />
          <div className='error-message w-full'>
            {errors?.tipoFracao?.message ? String(errors.tipoFracao?.message) : ''}
          </div>
        </div>

        <input
          type='submit'
          value='Criar'
          className='min-w-20 cursor-pointer rounded px-4 font-semibold'
        />
        <Link to={'..'} relative='path'>
          <button
            type='button'
            className='cursor-pointer rounded bg-red-100 px-4 py-2 font-semibold text-red-500'
          >
            Cancelar
          </button>
        </Link>
      </form>
    </section>
  );
}

export default FracaoCriar;
