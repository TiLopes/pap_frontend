import { Link, useNavigate } from 'react-router-dom';
import { useMutation, useQuery } from '@tanstack/react-query';
import axiosInstance from '@helpers/axiosInstance';
import DataTable from '@components/DataTable';
import DataTablePagination from '@components/DataTablePagination';
import Modal from 'react-modal';
import Select from 'react-select';
import { useEffect, useState } from 'react';
import { useForm, Controller } from 'react-hook-form';
import { useSnackbar } from 'notistack';
import ConfirmationModal from '@components/ConfirmationModal';
import DatePicker from 'react-date-picker';
import 'react-date-picker/dist/DatePicker.css';
import 'react-calendar/dist/Calendar.css';

Modal.setAppElement('#root');

function AdminFracoes() {
  const navigate = useNavigate();
  const { enqueueSnackbar } = useSnackbar();

  function Redirect() {
    const path = '/login/condominio';
    navigate(path);
  }

  const [showConfirmation, setShowConfirmation] = useState(false);
  const [fracaoSelected, setFracaoSelected] = useState(null);
  const [dataCalendarioEditar, setDataCalendarioEditar] = useState(null);
  const [condominosOptions, setCondominosOptions] = useState([]);
  const [estadoSelect, setEstadoSelect] = useState();
  const [selectedInfoRow, setSelectedInfoRow] = useState(null);
  const [selectedEditRow, setSelectedEditRow] = useState(null);
  const [infoModalClose, setInfoModalClose] = useState(false);
  const [infoIsOpen, setInfoIsOpen] = useState(false);
  const [editModalClose, setEditModalClose] = useState(false);
  const [editIsOpen, setEditIsOpen] = useState(false);
  const [pageData, setPageData] = useState({
    rowData: [],
    isLoading: true,
    totalPages: 0,
    totalFracoes: 0,
  });
  const [refreshData, setRefreshData] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);
  const [editModalSelects, setEditModalSelects] = useState({
    estado: null,
    tipo_fracao: null,
    ocupante: null,
  });
  const columns = [
    {
      // nome de cabeçalho
      Header: 'Fração',
      // nome da propriedade que o react-table vai buscar a informação
      accessor: 'id_fracao',
      // tamanho da coluna
      width: 100,
    },
    {
      Header: 'Ocupante',
      accessor: 'condomino',
      width: 200,
    },
    {
      Header: 'Permilagem escritura',
      accessor: 'permilagem_escritura',
      width: 200,
    },
    {
      Header: 'Tipo',
      accessor: 'tipo',
      width: 150,
    },
    {
      Header: 'Andar',
      accessor: 'andar',
      width: 100,
    },
    {
      Header: 'Data de aquisição',
      accessor: 'data_aquisicao',
      width: 175,
    },

    {
      Header: 'Estado',
      accessor: 'estado',
      width: 100,
    },
    {
      Header: 'Ações',
      accessor: 'acoes',
      Cell: (props) => (
        <div className='flex items-center justify-evenly'>
          <button
            className='flex items-center border-none bg-transparent'
            onClick={async () => {
              try {
                const res = await axiosInstance.get(
                  `/api/get/fracao/${props?.row.original?.id_fracao}`,
                );
                setSelectedInfoRow(res.data);
                openInfoModal();
              } catch (err) {
                console.error(err);
                if (err.response.status === 403 || err.response.status === 401) {
                  Redirect();
                }
              }
            }}
          >
            <i className='bx bx-search text-xl text-sky-400' title='Ver' />
          </button>
          <button
            className='flex items-center border-none bg-transparent'
            onClick={async () => {
              try {
                const res = await axiosInstance.get(
                  `/api/get/fracao/${props?.row?.original.id_fracao}`,
                );

                const data = res.data;

                setEditModalSelects({
                  estado: { value: data?.fracao.estado, label: data?.fracao.estado },
                  ocupante: {
                    value: data?.fracao.id_condomino_condomino?.id,
                    label: data?.fracao.id_condomino_condomino?.nome_ocupante,
                  },
                  tipo_fracao: {
                    value: data?.fracao.tipo_tipos_fracao.id,
                    label: data?.fracao.tipo_tipos_fracao.descricao,
                  },
                });

                // alterar valor para o da linha clicada
                setSelectedEditRow(data);
              } catch (err) {
                console.error(err);
                if (err.response.status === 403 || err.response.status === 401) {
                  Redirect();
                }
              }
            }}
          >
            <i className='bx bxs-edit text-xl text-green-400' title='Editar'></i>
          </button>
          <button
            className='flex items-center border-none bg-transparent'
            onClick={() => {
              setFracaoSelected(props?.row.original?.id_fracao);
              setShowConfirmation(true);
            }}
          >
            <i className='bx bx-trash text-xl text-red-500' title='Eliminar'></i>
          </button>
        </div>
      ),
      minWidth: 120,
    },
  ];
  const deleteFracao = useMutation({
    mutationFn: async (id) => {
      return await axiosInstance.post('/api/delete/fracao', { id });
    },
    onSuccess: () => {
      enqueueSnackbar('Elimininada com sucesso', {
        variant: 'success',
      });
      setRefreshData(true);
    },
    onError: (err) => {
      enqueueSnackbar('Oops! Tente novamente.', {
        variant: 'error',
      });
      console.error(err);
      if (err.response.status === 403 || err.response.status === 401) {
        Redirect();
      }
    },
  });

  async function getCondominosInfo() {
    return await axiosInstance.get('/api/get/condominos/basic');
  }

  const condominosOptionsData = useQuery({
    queryKey: ['condominos', 'fracoes'],
    queryFn: getCondominosInfo,
    onSuccess: (res) => {
      for (const condomino of res.data.condominos) {
        setCondominosOptions((prev) => [
          ...prev,
          { value: condomino.id, label: condomino.nome_ocupante },
        ]);
      }
    },
    onError: (error) => {
      if (error.response.status === 403 || error.response.status === 401) {
        Redirect();
      }
    },
  });

  // opções dos tipos das frações
  const [optionsTipoFracao, setOptionsTipoFracao] = useState([]);

  const tiposFracao = useQuery({
    queryFn: async () => {
      return await axiosInstance.get('/api/tipos/fracoes');
    },
    queryKey: ['fracoes', 'criar'],
    onSuccess: (res) => {
      for (const tipo of res.data.tipos) {
        setOptionsTipoFracao((prev) => [...prev, { value: tipo.id, label: tipo.descricao }]);
      }
    },
  });

  const getData = async (pageNo = 0) => {
    const response = await axiosInstance(
      `/api/get/fracoes?limit=10&offset=${pageNo * 10}&order=id&dir=ASC`,
    );

    return response;
  };

  // quando mudar de página "buscar" as próximas 10 ocorrências
  useEffect(() => {
    setPageData((prevState) => ({
      ...prevState,
      isLoading: true,
    }));

    getData(currentPage)
      .then((info) => {
        const { data } = info;
        const totalFracoes = data.count;
        const totalPages = Math.floor(totalFracoes / 10);
        setTimeout(
          () => {
            setPageData({
              isLoading: false,
              rowData: data.fracoes,
              totalPages,
              totalFracoes,
            });
          },
          refreshData ? '' : 250,
        );
      })
      .catch((err) => {
        if (err.response.status === 403 || err.response.status === 401) {
          Redirect();
        }
      });

    return () => setRefreshData(false);
  }, [currentPage, refreshData]);

  useEffect(() => {
    if (selectedEditRow !== null) openEditModal();
  }, [selectedEditRow]);

  function openInfoModal() {
    setInfoModalClose(false);
    setInfoIsOpen(true);
  }

  function closeInfoModal() {
    setInfoModalClose(true);
    setTimeout(() => {
      setInfoIsOpen(false);
    }, 50);
  }

  function openEditModal() {
    reset();
    console.log(editModalSelects);
    setEditModalClose(false);
    setDataCalendarioEditar(selectedEditRow?.fracao.data_aquisicao);
    setEditIsOpen(true);
  }

  function closeEditModal() {
    setDataCalendarioEditar(null);
    setEstadoSelect(null);
    setEditModalClose(true);
    setTimeout(() => {
      setEditIsOpen(false);
    }, 50);
  }

  const {
    register: registerEditar,
    handleSubmit: handleSubmitEditar,
    control: controlEditar,
    reset,
  } = useForm({
    defaultValues: {
      data_aquisicao: null,
    },
  });

  const editarFracao = useMutation({
    mutationFn: async (data) => {
      console.log(data);
      await axiosInstance.post(`/api/edit/fracao/${data.id_fracao}`, {
        andar: data.andar,
        escritura: Number(data.permilagem_escritura),
        rateio: Number(data.permilagem_rateio),
        estado: data.estado.value,
        tipo: data.tipo_fracao.value,
        ocupante: data.ocupante?.value,
        data_aquisicao: data.data_aquisicao,
      });
    },
    onSuccess: () => {
      enqueueSnackbar('Editada com sucesso', {
        variant: 'success',
      });
      setRefreshData(true);
      closeEditModal();
    },
    onError: (error) => {
      console.error(error);
      enqueueSnackbar('Oops. Tente novamente', {
        variant: 'error',
      });
    },
  });

  function onSubmitEditar(form) {
    console.log(editModalSelects);
    console.log(dataCalendarioEditar);
    form['estado'] = editModalSelects.estado;
    form['ocupante'] = editModalSelects.ocupante;
    form['tipo_fracao'] = editModalSelects.tipo_fracao;
    editarFracao.mutate(form);
  }

  return (
    <>
      <ConfirmationModal
        message='Tem certeza que deseja eliminar esta fração?'
        confirmModalIsOpen={showConfirmation}
        onCloseModal={() => setShowConfirmation(false)}
        onConfirmClick={() => deleteFracao.mutate(fracaoSelected)}
      />
      <section className='fracoes mx-[clamp(16px,3svw,32px)]'>
        <h1 className='mb-8'>Frações</h1>
        <Link to='criar' className='rounded-lg bg-[#1d1b31] p-4 font-semibold text-white'>
          Adicionar fração
        </Link>

        <section className='ver-fracoes mt-12'>
          <div className='max-h-fit'>
            <DataTable columns={columns} data={pageData.rowData} isLoading={pageData.isLoading} />
          </div>
          <DataTablePagination
            totalRows={pageData.totalOcorrencias}
            pageChangeHandler={setCurrentPage}
            rowsPerPage={10}
          />
          {pageData.totalFracoes == 0 && !pageData.isLoading && (
            <div className='mt-8 w-full'>
              <h2 className='prose mx-auto text-center text-xl'>Não existem frações</h2>
            </div>
          )}
          {/* VER FRAÇÃO MODAL */}
          <Modal
            isOpen={infoIsOpen}
            onRequestClose={closeInfoModal}
            className={`Modal ${infoModalClose ? 'close' : ''} dark-theme`}
            overlayClassName='Overlay'
            id='modal'
          >
            <fieldset className='Fieldset'>
              <label className='Label' htmlFor='id'>
                Fração
              </label>
              <input
                type='text'
                className='Input'
                name='id'
                disabled
                defaultValue={selectedInfoRow?.fracao.id}
              />
            </fieldset>
            <fieldset className='Fieldset'>
              <label className='Label' htmlFor='tipo'>
                Tipo da fração
              </label>
              <input
                type='text'
                className='Input'
                name='tipo'
                disabled
                defaultValue={selectedInfoRow?.fracao.tipo_tipos_fracao.descricao}
              />
            </fieldset>
            <fieldset className='Fieldset'>
              <label className='Label' htmlFor='andar'>
                Andar
              </label>
              <input
                type={'text'}
                className='Input'
                name='andar'
                disabled
                defaultValue={selectedInfoRow?.fracao.andar}
              />
            </fieldset>
            <fieldset className='Fieldset'>
              <label className='Label' htmlFor='escritura'>
                Escritura
              </label>
              <input
                type={'text'}
                className='Input'
                name='escritura'
                disabled
                defaultValue={selectedInfoRow?.fracao.permilagem_escritura}
              />
            </fieldset>
            <fieldset className='Fieldset'>
              <label className='Label' htmlFor='escritura'>
                Rateio
              </label>
              <input
                type={'text'}
                className='Input'
                name='rateio'
                disabled
                defaultValue={selectedInfoRow?.fracao.permilagem_rateio}
              />
            </fieldset>
            <fieldset className='Fieldset'>
              <label className='Label' htmlFor='estado'>
                Estado
              </label>
              <input
                type={'text'}
                className='Input'
                name='estado'
                disabled
                defaultValue={selectedInfoRow?.fracao.estado}
              />
            </fieldset>
            <fieldset className='Fieldset'>
              <label className='Label' htmlFor='andar'>
                Data de aquisição
              </label>
              <input
                type={'text'}
                className='Input'
                name='data_aquisicao'
                disabled
                defaultValue={selectedInfoRow?.fracao.data_aquisicao}
              />
            </fieldset>
            <fieldset className='Fieldset'>
              <label className='Label' htmlFor='ocupante'>
                Ocupante
              </label>
              <input
                type={'text'}
                className='Input'
                name='ocupante'
                disabled
                defaultValue={selectedInfoRow?.fracao.id_condomino_condomino?.nome_ocupante}
              />
            </fieldset>
            <button onClick={closeInfoModal} className='Button red'>
              Fechar
            </button>
          </Modal>
          {/* EDITAR FRAÇÃO MODAL */}
          <Modal
            isOpen={editIsOpen}
            onRequestClose={closeEditModal}
            className={`Modal ${editModalClose ? 'close' : ''} dark-theme`}
            overlayClassName='Overlay'
            id='modal-edit'
          >
            <form onSubmit={handleSubmitEditar(onSubmitEditar)}>
              <fieldset className='Fieldset'>
                <label className='Label' htmlFor='fracao'>
                  Fração
                </label>
                <input
                  type={'text'}
                  className='Input'
                  name='fracao'
                  readOnly
                  defaultValue={selectedEditRow?.fracao.id}
                  {...registerEditar('id_fracao')}
                />
              </fieldset>
              <fieldset className='Fieldset'>
                <label className='Label' htmlFor='tipo'>
                  Tipo da fração
                </label>
                <Select
                  options={optionsTipoFracao}
                  inputId='tipo_fracao'
                  className='w-full'
                  placeholder='Indique o tipo da fração'
                  defaultValue={{
                    value: selectedEditRow?.fracao.tipo_tipos_fracao.id,
                    label: selectedEditRow?.fracao.tipo_tipos_fracao.descricao,
                  }}
                  {...registerEditar('tipo_fracao')}
                  onChange={(selected) =>
                    setEditModalSelects((prev) => ({
                      ...prev,
                      tipo_fracao: selected,
                    }))
                  }
                />
              </fieldset>
              <fieldset className='Fieldset'>
                <label className='Label' htmlFor='andar'>
                  Andar
                </label>
                <input
                  type={'text'}
                  className='Input'
                  name='andar'
                  defaultValue={selectedEditRow?.fracao.andar}
                  {...registerEditar('andar')}
                />
              </fieldset>
              <fieldset className='Fieldset'>
                <label className='Label' htmlFor='escritura'>
                  Escritura
                </label>
                <input
                  type={'text'}
                  className='Input'
                  name='permilagem_escritura'
                  defaultValue={selectedEditRow?.fracao.permilagem_escritura}
                  {...registerEditar('permilagem_escritura')}
                />
              </fieldset>
              <fieldset className='Fieldset'>
                <label className='Label' htmlFor='escritura'>
                  Rateio
                </label>
                <input
                  type={'text'}
                  className='Input'
                  name='permilagem_rateio'
                  defaultValue={selectedEditRow?.fracao.permilagem_rateio}
                  {...registerEditar('permilagem_rateio')}
                />
              </fieldset>
              <fieldset className='Fieldset'>
                <label className='Label' htmlFor='estado'>
                  Estado
                </label>
                <Select
                  inputId='estado'
                  options={[
                    { value: 'Livre', label: 'Livre' },
                    { value: 'Ocupado', label: 'Ocupado' },
                  ]}
                  defaultValue={editModalSelects.estado}
                  onChange={(selected) =>
                    setEditModalSelects((prev) => ({
                      ...prev,
                      estado: selected,
                    }))
                  }
                  name='estado'
                  value={
                    estadoSelect || {
                      value: selectedEditRow?.fracao.estado,
                      label: selectedEditRow?.fracao.estado,
                    }
                  }
                  {...registerEditar('estado')}
                />
              </fieldset>
              <fieldset className='Fieldset'>
                <label className='Label' htmlFor='andar'>
                  Data de aquisição
                </label>
                <Controller
                  control={controlEditar}
                  name='data_aquisicao'
                  render={({ field: { onChange, value } }) => (
                    <DatePicker
                      defaultValue={selectedInfoRow?.fracao.data_aquisicao}
                      className={'w-48 rounded  border border-gray-300 p-1.5'}
                      onChange={onChange}
                      value={value ? value : selectedEditRow?.fracao.data_aquisicao}
                      locale={'pt-PT'}
                      calendarClassName={'min-w-120'}
                      showLeadingZeros
                    />
                  )}
                />
              </fieldset>
              <fieldset className='Fieldset'>
                <label className='Label' htmlFor='ocupante'>
                  Ocupante
                </label>
                <Select
                  inputId='ocupante'
                  options={condominosOptions}
                  name='ocupante'
                  isClearable={true}
                  defaultValue={
                    selectedEditRow?.fracao.id_condomino_condomino
                      ? {
                          value: selectedEditRow?.fracao.id_condomino_condomino?.id,
                          label: selectedEditRow?.fracao.id_condomino_condomino?.nome_ocupante,
                        }
                      : null
                  }
                  {...registerEditar('ocupante')}
                  onChange={(selected) => {
                    if (!selected) {
                      setEditModalSelects((prev) => ({
                        ...prev,
                        estado: {
                          value: 'Livre',
                          label: 'Livre',
                        },
                        ocupante: selected,
                      }));
                      setEstadoSelect({ value: 'Livre', label: 'Livre' });
                    } else {
                      setEditModalSelects((prev) => ({
                        ...prev,
                        estado: {
                          value: 'Ocupado',
                          label: 'Ocupado',
                        },
                        ocupante: selected,
                      }));
                      setEstadoSelect({ value: 'Ocupado', label: 'Ocupado' });
                    }
                  }}
                />
              </fieldset>
              <div className='flex'>
                <button type='submit' className='Button green'>
                  Guardar
                </button>
                <button onClick={closeEditModal} className='Button red' type='button'>
                  Fechar
                </button>
              </div>
            </form>
          </Modal>
        </section>
      </section>
    </>
  );
}

export default AdminFracoes;
