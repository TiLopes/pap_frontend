import { useNavigate } from 'react-router-dom';
import axiosInstance from '@helpers/axiosInstance';
import { useQuery } from '@tanstack/react-query';
import '@styles/AdminProfile.scss';

const Perfil = () => {
  const navigate = useNavigate(); // hook de navegação de páginas

  // fazer um pedido a API
  const info = useQuery({
    // chave única do pedido
    queryKey: ['administracao', 'perfil'],
    // função que faz pedido
    queryFn: async () => {
      const res = await axiosInstance.get('/api/condominio/info');
      // devolver a informação
      return res.data.condominio;
    },
    // se der erro
    onError: (err) => {
      // se não tiver permissão redirecionar para a página inicial
      if (err.response.status === 401 || err.response.status === 403) {
        navigate('/');
        return;
      }
    },
  });

  // renderizar a página se o pedido foi feito com sucesso
  if (info.isSuccess)
    return (
      <section className='mx-[clamp(16px,3svw,32px)] w-full'>
        <h1 className='prose mb-6 text-4xl font-bold mt-4'>Perfil</h1>
        <div className='md-w-2xl border-rd-md h-fit w-full border p-8'>
          <fieldset className='mb-6 flex w-full flex-col'>
            <label className='prose text-md mb-2 font-bold'>Nome</label>
            <input
              disabled
              type='text'
              className='bordered border-rd w-full border border-neutral-200 bg-zinc-100 p-2'
              defaultValue={info.data.nome}
            />
          </fieldset>
          <div className='md-flex-nowrap flex flex-wrap justify-between'>
            <fieldset className='md-w-fit mb-6 flex w-full flex-col'>
              <label className='prose text-md mb-2 font-bold'>NIF</label>
              <input
                disabled
                type='text'
                className='bordered border-rd md-w-fit w-full border border-neutral-200 bg-zinc-100 p-2'
                defaultValue={info.data.nif}
              />
            </fieldset>
            <fieldset className='md-w-fit mb-6 flex w-full flex-col'>
              <label className='prose text-md mb-2 font-bold'>Nº telemóvel</label>
              <input
                disabled
                type='text'
                className='bordered border-rd md-w-fit w-full border border-neutral-200 bg-zinc-100 p-2'
                defaultValue={info.data.telemovel_admin}
              />
            </fieldset>
          </div>
          <fieldset className='mb-6 flex w-full flex-col'>
            <label className='prose text-md mb-2 font-bold'>Email</label>
            <input
              disabled
              type='text'
              className='bordered border-rd w-full border border-neutral-200 bg-zinc-100 p-2'
              defaultValue={info.data.email_admin}
            />
          </fieldset>
          <fieldset className='mb-6 flex w-full flex-col'>
            <label className='prose text-md mb-2 font-bold'>Nome do administrador</label>
            <input
              disabled
              type='text'
              className='bordered border-rd md-w-fit w-full border border-neutral-200 bg-zinc-100 p-2'
              defaultValue={info.data.nome_admin}
            />
          </fieldset>
          <div className='md-flex-nowrap md-gap-8 flex w-full flex-wrap justify-between'>
            <fieldset className='md-w-md mb-6 flex w-full flex-col'>
              <label className='prose text-md mb-2 font-bold'>Morada</label>
              <input
                disabled
                type='text'
                className='bordered border-rd w-full border border-neutral-200 bg-zinc-100 p-2'
                defaultValue={info.data.morada}
              />
            </fieldset>
            <fieldset className='mb-6 flex flex-col'>
              <label className='prose text-md mb-2 font-bold'>Código postal</label>
              <input
                disabled
                type='text'
                className='bordered border-rd md-w-fit w-full border border-neutral-200 bg-zinc-100 p-2'
                defaultValue={info.data.cod_postal}
              />
            </fieldset>
          </div>
          <div className='md-flex-nowrap md-gap-16 gap-6 mb-6 flex w-full flex-wrap justify-between'>
            <fieldset className='md-w-md flex w-full flex-col'>
              <label className='prose text-md mb-2 font-bold'>Orçamento anual</label>
              <input
                disabled
                type='text'
                className='bordered border-rd w-full border border-neutral-200 bg-zinc-100 p-2'
                defaultValue={info.data.orcamento_anual + '€'}
              />
            </fieldset>
            <fieldset className='md-w-md flex w-full flex-col'>
              <label className='prose text-md mb-2 font-bold'>Dia de pagamento de quotas</label>
              <input
                disabled
                type='text'
                className='bordered border-rd w-14 border border-neutral-200 bg-zinc-100 p-2'
                defaultValue={info.data.dia_pagamento_quota}
              />
            </fieldset>
          </div>
        </div>
      </section>
    );
};

export default Perfil;
