import { Controller, useForm } from 'react-hook-form';
import '@styles/AdminCondomino.scss';
import { useMutation, useQuery } from '@tanstack/react-query';
import { useNavigate } from 'react-router-dom';
import Select from 'react-select';
import axiosInstance from '@helpers/axiosInstance';
import { useSnackbar } from 'notistack';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';
import { useState } from 'react';
import DatePicker from 'react-date-picker';

import 'react-date-picker/dist/DatePicker.css';
import 'react-calendar/dist/Calendar.css';

function Criar() {
  const navigate = useNavigate();
  const [fracoesOptions, setFracoesOptions] = useState([]);
  const { enqueueSnackbar } = useSnackbar();

  async function getInfo() {
    return axiosInstance.get('/api/get/fracoeslivres');
  }

  function Redirect() {
    const path = '/login/condominio';
    navigate(path);
  }

  const {
    isError,
    isSuccess,
    data: queryData,
  } = useQuery({
    queryKey: ['condomino', 'criar'],
    queryFn: getInfo,
    onSuccess: ({ data }) => {
      for (const fracao of data.fracoes) {
        setFracoesOptions((prev) => [...prev, { label: fracao.id, value: fracao.id }]);
      }
    },
    onError: ({ response }) => {
      if (response.status === 401 || response.status === 403) {
        navigate('..', { relative: 'path' });
      }
    },
  });

  const validationSchema = Yup.object({
    fracao: Yup.array().min(1, 'Preencha este campo').required('Preencha este campo'),
    aquisicao: Yup.date()
      .typeError('Data inválida')
      .max(new Date(), 'Escolha uma data até presente')
      .required('Preencha este campo'),
    nome: Yup.string().required('Preencha este campo'),
    nif: Yup.string()
      .matches(/^\d{9}$/, 'NIF inválido')
      .required('Preencha este campo'),
    telemovel: Yup.string()
      .matches(/\+\d{1,5} \d{9}$/, 'Número telemóvel inválido')
      .required('Preencha este campo'),
    email: Yup.string().email('Email inválido').required('Preencha este campo'),
  });

  const {
    register,
    handleSubmit,
    control,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(validationSchema),
    mode: 'all',
  });

  const addCondomino = useMutation({
    mutationFn: (data) => {
      return axiosInstance.put('/api/create/condomino', {
        fracao: data.fracao,
        aquisicao: data.aquisicao,
        nome: data.nome,
        nif: data.nif,
        telemovel: data.telemovel,
        email: data.email,
      });
    },
    onSuccess: () => {
      enqueueSnackbar('Adicionado com sucesso!', { variant: 'success' });
      navigate('..', { relative: 'path' });
    },
    onError: () => {
      enqueueSnackbar('Oops. Tente novamente', {
        variant: 'error',
      });
    },
  });

  const onSubmit = (formData) => {
    addCondomino.mutate({
      fracao: formData.fracao,
      aquisicao: formData.aquisicao.toISOString().slice(0, 10),
      nome: formData.nome,
      nif: formData.nif,
      telemovel: formData.telemovel,
      email: formData.email,
    });
  };

  if (isError) {
    Redirect();
    return;
  }

  if (isSuccess)
    return (
      <section className='condomino-criar'>
        <h1>Adicionar condómino</h1>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className='input-wrapper !w-3/5'>
            <label>Fração</label>
            <Controller
              render={({ field }) => (
                <Select
                  {...field}
                  isMulti
                  options={fracoesOptions}
                  className='w-full'
                  isClearable={true}
                  placeholder='Indique a fração / as frações'
                />
              )}
              name='fracao'
              control={control}
            />
            <div className='error-message w-full'>
              {errors?.fracao?.message ? String(errors.fracao?.message) : ''}
            </div>
          </div>
          <div className='input-wrapper !mb-3 flex items-center justify-between'>
            <div className='input-wrapper flex flex-col pr-6'>
              <label>Data de aquisição</label>
              <Controller
                control={control}
                name='aquisicao'
                render={({ field: { onChange, value } }) => (
                  <DatePicker
                    className={'w-48 rounded  border border-gray-300 p-1.5'}
                    onChange={onChange}
                    value={value}
                    locale={'pt-PT'}
                    calendarClassName={'min-w-120'}
                    clearIcon={null}
                    showLeadingZeros
                    maxDate={new Date()}
                  />
                )}
              />
              <div className='error-message w-full'>
                {errors.aquisicao?.message ? String(errors.aquisicao?.message) : ''}
              </div>
            </div>
          </div>
          <div className='input-wrapper !w-4/6'>
            <label htmlFor='nome'>Nome do condómino</label>
            <input type='text' name='nome' {...register('nome')} />
            <div className='error-message w-full'>
              {errors.nome?.message ? String(errors.nome?.message) : ''}
            </div>
          </div>
          <div className='input-wrapper !w-1/4'>
            <label htmlFor='nif'>NIF</label>
            <input
              type='text'
              name='nif'
              maxLength={9}
              size={9}
              placeholder='123456789'
              {...register('nif')}
            />
            <div className='error-message w-full'>
              {errors.nif?.message ? String(errors.nif?.message) : ''}
            </div>
          </div>
          <div className='input-wrapper !w-4/6'>
            <label>Morada</label>
            <input
              type='text'
              {...register('morada')}
              defaultValue={queryData.data.condominio.morada}
              readOnly
            />
          </div>
          <div className='input-wrapper !w-2/6 pl-4'>
            <label>Código Postal</label>
            <input
              type='text'
              {...register('codPostal')}
              defaultValue={queryData.data.condominio.cod_postal}
              readOnly
            />
          </div>
          <div className='input-wrapper'>
            <label>Telemóvel</label>
            <input
              type='text'
              maxLength={15}
              placeholder='+351 912345678'
              {...register('telemovel')}
            />
            <div className='error-message w-full'>
              {errors.telemovel?.message ? String(errors.telemovel?.message) : ''}
            </div>
          </div>
          <div className='input-wrapper '>
            <label>Email</label>
            <input type='text' {...register('email')} />
            <div className='error-message w-full'>
              {errors.email?.message ? String(errors.email?.message) : ''}
            </div>
          </div>
          <div className='input-wrapper flex justify-evenly'>
            <input type='submit' value='Adicionar' className='cursor-pointer !px-4 font-semibold' />
            <input
              type='button'
              onClick={() =>
                navigate('..', {
                  relative: 'path',
                })
              }
              value='Cancelar'
              className='cursor-pointer !bg-red-100 !px-4 font-semibold !text-red-500'
            />
          </div>
        </form>
      </section>
    );
}

export default Criar;
