import { Link, useNavigate } from 'react-router-dom';
import { useMutation } from '@tanstack/react-query';
import DataTable from '@components/DataTable';
import DataTablePagination from '@components/DataTablePagination';
import { useEffect, useState } from 'react';
import axiosInstance from '@helpers/axiosInstance';
import ConfirmationModal from '@components/ConfirmationModal';
import ReactModal from 'react-modal';

ReactModal.setAppElement('#root');

function AdminCondomino() {
  const navigate = useNavigate();
  const [pageData, setPageData] = useState({
    rowData: [],
    isLoading: true,
    totalPages: 0,
    totalCondominos: 0,
  });
  const [showConfirmation, setShowConfirmation] = useState(false);
  const [condominoSelected, setCondominoSelected] = useState(null);
  const [refreshData, setRefreshData] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);
  const columns = [
    {
      Header: 'ID',
      accessor: 'id',
    },
    {
      Header: 'Nome',
      accessor: 'nome_ocupante',
      width: 250,
    },
    {
      Header: 'NIF',
      accessor: 'nif_ocupante',
      width: 100,
    },
    {
      Header: 'Nº telemóvel',
      accessor: 'telemovel_ocupante',
      width: 150,
    },
    {
      Header: 'Email',
      accessor: 'email_ocupante',
      width: 250,
    },
    {
      Header: 'Nome de utilizador',
      accessor: 'username',
      width: 250,
    },
    {
      Header: 'Ações',
      id: 'acoes',
      Cell: ({ row: { original } }) => {
        return (
          <div className='flex w-full justify-evenly'>
            <button
              className='bg-transparent p-2'
              title='Banir'
              onClick={() => {
                setCondominoSelected(original.id);
                setShowConfirmation(true);
              }}
            >
              <i className='bx bx-trash text-xl text-red-500'></i>
            </button>
          </div>
        );
      },
    },
  ];

  const banirCondomino = useMutation({
    mutationFn: async (id) => {
      await axiosInstance.post('/api/ban/condomino', {
        id,
      });
    },
    onSuccess: () => setRefreshData(true),
  });

  const getData = async (pageNo = 0) => {
    const response = await axiosInstance(
      `/api/get/condominos?limit=10&offset=${pageNo * 10}&order=id&dir=ASC`,
    );

    return response;
  };

  useEffect(() => {
    setPageData((prevState) => ({
      ...prevState,
      isLoading: true,
    }));

    getData(currentPage)
      .then((info) => {
        const { data } = info;
        const totalCondominos = data.count;
        const totalPages = Math.floor(totalCondominos / 10);
        setTimeout(
          () => {
            setPageData({
              isLoading: false,
              rowData: data.condominos,
              totalPages,
              totalCondominos,
            });
          },
          refreshData ? 0 : 250,
        );
      })
      .catch((err) => {
        if (err.response.status === 403 || err.response.status === 401) {
          navigate('/');
        }
      });

    return () => setRefreshData(false);
  }, [currentPage, refreshData]);

  return (
    <section className='condominos mx-[clamp(16px,3svw,32px)] w-full'>
      <h1 className='prose mb-8'>Condóminos</h1>
      <Link to='criar' className='rounded-lg bg-[#1d1b31] p-4 text-white font-semibold'>
        Adicionar condómino
      </Link>
      <section className='ver-condominos mt-12 w-full'>
        <div className='md-overflow-x-visible max-h-fit w-full overflow-x-auto'>
          <p className='prose mb-4'>
            Total de condóminos: <span className='font-bold'>{pageData.totalCondominos}</span>
          </p>
          <DataTable columns={columns} data={pageData.rowData} isLoading={pageData.isLoading} />
          {pageData.totalCondominos == 0 && !pageData.isLoading && (
            <div className='mt-8 w-full'>
              <h2 className='prose mx-auto text-center text-xl'>Não existem condóminos</h2>
            </div>
          )}
        </div>
        <DataTablePagination
          totalRows={pageData.totalOcorrencias}
          pageChangeHandler={setCurrentPage}
          rowsPerPage={10}
        />
      </section>
      <ConfirmationModal
        message='Tem certeza que deseja banir/eliminar este condómino?'
        confirmModalIsOpen={showConfirmation}
        onCloseModal={() => setShowConfirmation(false)}
        onConfirmClick={() => banirCondomino.mutate(condominoSelected)}
      />
    </section>
  );
}

export default AdminCondomino;
