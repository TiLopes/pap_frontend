import { useEffect, useMemo, useState, forwardRef, useRef } from 'react';
import { Controller, useForm } from 'react-hook-form';
import Select from 'react-select';
import { useMutation, useQuery } from '@tanstack/react-query';
import axiosInstance from '@helpers/axiosInstance';
import { format } from 'date-fns';
import { usePagination, useTable, useRowSelect } from 'react-table';
import CurrencyInput from 'react-currency-input-field';
import pt from 'date-fns/locale/pt';
import { useNavigate } from 'react-router-dom';
import '@styles/AdminMovimentos.scss';
import '@styles/DataTable.scss';
import '@styles/DataTablePagination.scss';
import DatePicker from 'react-date-picker';

import 'react-date-picker/dist/DatePicker.css';
import 'react-calendar/dist/Calendar.css';
import { useSnackbar } from 'notistack';
import { Link } from 'react-router-dom';
import LoadingSpinner from '@components/LoadingSpinner';

function RegistarPagamentoQuota() {
  const navigate = useNavigate();
  const [quotasCondomino, setQuotasCondomino] = useState(null);
  const [condominoAtivo, setCondominoAtivo] = useState(null);
  const [refetch, setRefetch] = useState(-1);
  const [condominosOptions, setCondominosOptions] = useState([]);
  const { isSuccess, isLoading, error, isError } = useQuery({
    queryKey: ['quotas', 'pagamento'],
    queryFn: async () => {
      const res = await axiosInstance.get('/api/get/condominos/basic');
      return res;
    },
    onSuccess: (res) => {
      for (const condomino of res.data.condominos) {
        setCondominosOptions((prev) => [
          ...prev,
          { value: condomino.id, label: condomino.nome_ocupante },
        ]);
      }
    },
  });

  const getQuotasCondominoPagar = async (id_condomino) => {
    const res = await axiosInstance.get(`/api/get/quotas/ativas?id_condomino=${id_condomino}`);
    setQuotasCondomino(res.data.quotas);
  };

  useEffect(() => {
    if (refetch < 0) return;

    getQuotasCondominoPagar(refetch);
  }, [refetch]);

  if (isError) {
    console.warn(error);
    return;
  }

  if (isLoading) {
    return <LoadingSpinner />;
  }

  if (isSuccess)
    return (
      <section className='mx-[clamp(16px,3svw,32px)] w-full'>
        <div className='mb-6 flex flex-wrap items-center gap-6 md:flex-nowrap'>
          <button className='bg-transparent' onClick={() => navigate('..')}>
            <i className='bx bx-arrow-back prose align-sub text-4xl'></i>
          </button>
          <h1 className='prose'>Registar pagamento</h1>
        </div>
        <div className='w-full rounded border border-solid border-gray-300 p-8'>
          <div className='input-wrapper'>
            <label htmlFor='condomino'>Condómino</label>
            <Select
              options={condominosOptions}
              onChange={(e) => {
                setCondominoAtivo(e.value);
                getQuotasCondominoPagar(e.value);
              }}
            />
          </div>
          {quotasCondomino ? (
            <div className='mt-6 w-full overflow-x-auto md:overflow-x-visible'>
              <TabelaQuotasCondomino
                data={quotasCondomino}
                condomino={condominoAtivo}
                onRefetch={(id) => setRefetch(id)}
              />
            </div>
          ) : (
            ''
          )}
        </div>
      </section>
    );
}

const IndeterminateCheckbox = forwardRef(({ indeterminate, ...rest }, ref) => {
  const defaultRef = useRef();
  const resolvedRef = ref || defaultRef;

  useEffect(() => {
    resolvedRef.current.indeterminate = indeterminate;
  }, [resolvedRef, indeterminate]);

  return (
    <>
      <input type='checkbox' ref={resolvedRef} {...rest} />
    </>
  );
});

IndeterminateCheckbox.displayName = 'permilagemCheckbox';

function TabelaQuotasCondomino({ data: dataRAW, condomino, onRefetch }) {
  const data = useMemo(() => dataRAW, [dataRAW]);
  const columns = useMemo(
    () => [
      {
        Header: 'Fração',
        accessor: 'id_fracao',
      },
      {
        Header: 'Valor',
        accessor: 'valor',
        Cell: ({ value }) => {
          return <>{Number(value).toFixed(2)}€</>;
        },
      },
      {
        Header: 'Período',
        accessor: 'data_vencimento',
        Cell: ({ value }) => {
          return (
            <>
              {typeof value === 'string'
                ? format(new Date(value), "MMMM 'de' yyyy", { locale: pt })
                : ''}
            </>
          );
        },
      },
    ],
    [],
  );
  const [precoTotal, setPrecoTotal] = useState(0);
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    selectedFlatRows,
    state: { pageIndex, selectedRowIds },
  } = useTable(
    {
      columns,
      data,
      initialState: {
        pageSize: 5,
      },
    },

    usePagination,
    useRowSelect,
    (hooks) => {
      hooks.visibleColumns.push((columns) => [
        // Let's make a column for selection
        ...columns,
        {
          id: 'selection',
          // The header can use the table's getToggleAllRowsSelectedProps method
          // to render a checkbox
          Header: ({ getToggleAllRowsSelectedProps }) => (
            <div className='flex items-center'>
              <IndeterminateCheckbox {...getToggleAllRowsSelectedProps()} />
              <span className='ml-4'>Pagar todas</span>
            </div>
          ),
          // The cell can use the individual row's getToggleRowSelectedProps method
          // to the render a checkbox
          Cell: ({ row }) => (
            <div className='flex items-center'>
              <IndeterminateCheckbox {...row.getToggleRowSelectedProps()} />
              <span className='ml-4'>Pagar</span>
            </div>
          ),
        },
      ]);
    },
  );
  const { register, handleSubmit, control } = useForm();
  const { enqueueSnackbar } = useSnackbar();

  const pagarQuotas = useMutation({
    mutationFn: ({ quotas, precoTotal: valor_total, observacoes, data, condomino }) => {
      return axiosInstance.post('/api/pagar/quota', {
        quotas: quotas,
        valor_total: valor_total,
        observacoes: observacoes,
        data: data,
        condomino: condomino,
      });
    },
    onSuccess: () => {
      enqueueSnackbar('Quota paga com sucesso.', {
        variant: 'success',
      });
      onRefetch(condomino);
    },
    onError: () => {
      enqueueSnackbar('Oops. Tente novamente.', {
        variant: 'error',
      });
    },
  });

  const onSubmit = async (form) => {
    if (precoTotal === 0) return;

    let quotasSelectionadas = [];

    for (const quota of selectedFlatRows) {
      quotasSelectionadas.push(quota.original.id);
    }

    pagarQuotas.mutate({
      quotas: quotasSelectionadas,
      valor_total: precoTotal,
      observacoes: form.observacoes,
      data: form.data,
      condomino: condomino,
    });
  };

  useEffect(() => {
    setPrecoTotal(0);

    for (const d of selectedFlatRows) {
      setPrecoTotal((precoAnterior) => precoAnterior + Number(d.original.valor));
    }
  }, [selectedRowIds, selectedFlatRows]);

  return (
    <div className='w-full min-w-[400px]'>
      <table {...getTableProps()}>
        <thead>
          {headerGroups.map((headerGroup, i) => (
            <tr {...headerGroup.getHeaderGroupProps()} key={i}>
              {headerGroup.headers.map((column, j) => (
                <th {...column.getHeaderProps()} key={j}>
                  {column.render('Header')}
                </th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody {...getTableBodyProps()}>
          {page.map((row, i) => {
            prepareRow(row);
            return (
              <tr {...row.getRowProps()} key={i}>
                {row.cells.map((cell, j) => {
                  return (
                    <td {...cell.getCellProps()} key={j}>
                      {cell.render('Cell')}
                    </td>
                  );
                })}
              </tr>
            );
          })}
        </tbody>
      </table>

      {data.length == 0 && (
        <div className='mb-6 mt-2 w-full'>
          <h2 className='prose mx-auto text-center text-lg'>Não existem quotas por pagar</h2>
        </div>
      )}

      <div className='pagination w-full'>
        <div className='pagebuttons'>
          <button
            className='pageBtn !ml-0 !w-fit text-black disabled:text-gray-300'
            onClick={() => gotoPage(0)}
            disabled={!canPreviousPage}
            title='Voltar para a primeira página'
          >
            <i className='bx bx-chevron-left' />
            <i className='bx bx-chevron-left -ml-4' />
          </button>
          <button
            className='pageBtn text-black hover:cursor-pointer disabled:text-gray-300'
            onClick={() => previousPage()}
            disabled={!canPreviousPage}
          >
            <i className='bx bx-chevron-left' />
          </button>

          <button
            className='pageBtn text-black disabled:text-gray-300'
            onClick={() => nextPage()}
            disabled={!canNextPage}
          >
            <i className='bx bx-chevron-right' />
          </button>
          <button
            className='pageBtn !w-fit text-black disabled:text-gray-300'
            onClick={() => gotoPage(pageCount - 1)}
            disabled={!canNextPage}
          >
            <i className='bx bx-chevron-right' />
            <i className='bx bx-chevron-right -ml-4' />
          </button>
          <span className='ml-4 align-super'>
            Página <strong>{pageIndex + 1} </strong>
            de <strong>{pageOptions.length}</strong>
          </span>
        </div>
        <Select
          defaultValue={{ value: 5, label: 'Mostrar 5' }}
          onChange={(e) => {
            setPageSize(Number(e.value));
          }}
          options={[5, 10, 20].map((pageSize) => {
            return { value: pageSize, label: `Mostrar ${pageSize}` };
          })}
          className='ml-6 w-48'
        />
      </div>

      <form className='mt-10 flex flex-col flex-wrap' onSubmit={handleSubmit(onSubmit)}>
        <div className='grid-row-[1fr_1fr] md-grid-cols-[11rem_1fr] mb-6 grid items-center gap-2'>
          <label className='w-44 font-bold'>Valor total:</label>
          <CurrencyInput
            suffix='€'
            readOnly
            decimalScale={2}
            decimalsLimit={2}
            defaultValue={0}
            value={precoTotal}
            className='w-36 rounded border border-gray-300 bg-gray-100 p-1.5'
          />
        </div>
        <div className='grid-row-[1fr_1fr] md-grid-cols-[11rem_1fr] mb-6 grid items-center gap-2'>
          <label className='w-44 font-bold'>Data do pagamento:</label>
          <div className='relative w-fit'>
            <Controller
              control={control}
              name={'data'}
              render={({ field: { onChange, value } }) => (
                <DatePicker
                  className={'w-48 rounded  border border-gray-300 p-1.5'}
                  onChange={onChange}
                  value={value}
                  locale={'pt-PT'}
                  calendarClassName={'min-w-120'}
                  clearIcon={null}
                  showLeadingZeros
                  maxDate={new Date()}
                  required
                />
              )}
            />
          </div>
        </div>
        <div className='grid-row-[1fr_1fr] md-grid-cols-[11rem_1fr] mb-6 grid items-center gap-2'>
          <label className='w-44 font-bold'>Observações:</label>
          <textarea
            className='md-w-5/6 w-full resize-none rounded border border-gray-300 p-1.5'
            rows={2}
            {...register('observacoes')}
          />
        </div>
        <div className='md-gap-30 flex w-full justify-center'>
          <button className='rounded bg-[#11101d] p-2 px-3 font-semibold text-white' type='submit'>
            Confirmar
          </button>
          <Link to={'..'} relative='path'>
            <button
              type='button'
              className='cursor-pointer rounded bg-red-100 px-4 py-2 font-semibold text-red-500'
            >
              Cancelar
            </button>
          </Link>
        </div>
      </form>
    </div>
  );
}

export default RegistarPagamentoQuota;
