import axiosInstance from '@helpers/axiosInstance';
import { yupResolver } from '@hookform/resolvers/yup';
import { useEffect, useState } from 'react';
import { Controller } from 'react-hook-form';
import { useForm } from 'react-hook-form';
import Select from 'react-select';
import * as Yup from 'yup';
import { useNavigate } from 'react-router-dom';
import { addMonths, format, subDays } from 'date-fns';
import { Link } from 'react-router-dom';
import { useSnackbar } from 'notistack';
import { useMutation, useQuery } from '@tanstack/react-query';
import DatePicker from 'react-date-picker';

import 'react-date-picker/dist/DatePicker.css';
import 'react-calendar/dist/Calendar.css';
import '@styles/AdminQuotas.scss';

export default function Criar() {
  const [porPermilagem, setPorPermilagem] = useState(false);
  const [showQuotaWarning, setShowQuotaWarning] = useState(false);
  const navigate = useNavigate();
  const { enqueueSnackbar } = useSnackbar();

  const yupSchema = Yup.object({
    valor: Yup.number()
      .typeError('Valor inválido')
      .min(porPermilagem ? 0 : 1, 'Valor inválido'),
    data_inicio: Yup.date().required('Preencha este campo').typeError('Valor inválido'),
    data_vencimento: Yup.date().required('Preencha este campo').typeError('Valor inválido'),
    frequencia: Yup.object().typeError('Valor inválido').required('Preencha este campo'),
  });

  const getQuotas = useQuery({
    queryKey: ['quotas', 'criar'],
    queryFn: async () => {
      const res = await axiosInstance.get('/api/add/quota');
      return res;
    },
    onSuccess: (res) => {
      if (res.data.exists) setShowQuotaWarning(true);
    },
    onError: (error) => {
      if (error.status === 401 || error.status === 403) {
        navigate('/');
        return;
      }
    },
  });

  const criarQuota = useMutation({
    mutationFn: (data) => {
      return axiosInstance.post('/api/add/quota', {
        frequencia: data.frequencia.value,
        data_inicio: format(data.data_inicio, 'yyyy-MM-dd'),
        data_vencimento: format(data.data_vencimento, 'yyyy-MM-dd'),
        porPermilagem,
        valor: data.valor,
      });
    },
    onSuccess: () => {
      enqueueSnackbar('Quota criada com sucesso.', {
        variant: 'success',
      });
      navigate('..', { relative: 'path' });
    },
    onError: () => {
      enqueueSnackbar('Oops. Tente novamente.', {
        variant: 'error',
      });
    },
  });

  useEffect(() => {
    if (showQuotaWarning)
      enqueueSnackbar('Já existem quotas ativas', {
        variant: 'warning',
      });

    return () => setShowQuotaWarning(false);
  }, [showQuotaWarning]);

  const onSubmit = async (form) => {
    console.log(form.data_vencimento);
    if (form.data_vencimento <= new Date() || form.data_vencimento <= form.data_inicio) {
      setError('data_vencimento', { type: 'min', message: 'Data inválida' });
      return;
    }
    criarQuota.mutate(form);
  };

  const {
    register,
    handleSubmit,
    control,
    setError,
    setValue,
    getValues,
    clearErrors,
    formState: { errors },
  } = useForm({
    mode: 'all',
    resolver: yupResolver(yupSchema),
    defaultValues: {
      frequencia: null,
      valor: 0,
      data_inicio: null,
      data_vencimento: null,
    },
  });

  useEffect(() => {
    if (porPermilagem) clearErrors('valor');
  }, [porPermilagem]);

  return (
    <section className='criar-quota'>
      <h1 className='mb-4'>Criar quota</h1>
      <form className='form' onSubmit={handleSubmit(onSubmit)}>
        <div className='input-wrapper'>
          <label>Frequência</label>
          <Controller
            name='frequencia'
            control={control}
            defaultValue=''
            rules={{ required: true }}
            render={({ field }) => (
              <Select {...field} options={[{ value: 'mensal', label: 'Mensal' }]} />
            )}
          />
          <span className='error-message'>{errors?.frequencia?.message}</span>
        </div>
        <div className='flex w-full gap-20'>
          <div className='input-wrapper flex flex-col'>
            <label>Data início</label>
            <div className='relative  w-full'>
              <Controller
                control={control}
                name='data_inicio'
                render={({ field: { onChange, value } }) => (
                  <DatePicker
                    className={'w-48 rounded  border border-gray-300 p-1.5'}
                    onChange={(data) => {
                      onChange(data);
                      setValue(
                        'data_vencimento',
                        subDays(addMonths(getValues('data_inicio'), 1), 1),
                      );
                    }}
                    value={value}
                    locale={'pt-PT'}
                    calendarClassName={'min-w-120'}
                    clearIcon={null}
                    showLeadingZeros
                    minDate={new Date(format(addMonths(new Date(), 1), 'yyyy-MM-01'))}
                  />
                )}
              />
            </div>
            <span className='error-message'>{errors?.data_inicio?.message}</span>
          </div>
          <div className='input-wrapper flex flex-col'>
            <label>Data de vencimento</label>
            <div className='relative  w-full'>
              <Controller
                control={control}
                name='data_vencimento'
                render={({ field: { onChange, value } }) => (
                  <DatePicker
                    className={'w-48 rounded  border border-gray-300 p-1.5'}
                    onChange={onChange}
                    value={value}
                    locale={'pt-PT'}
                    calendarClassName={'min-w-120'}
                    clearIcon={null}
                    minDate={subDays(addMonths(getValues('data_inicio'), 1), 1)}
                  />
                )}
              />
            </div>
            <span className='error-message'>{errors?.data_vencimento?.message}</span>
          </div>
        </div>
        <div className='input-wrapper flex items-center'>
          <div className='flex w-1/3 flex-col'>
            <label htmlFor='valor'>Valor da quota</label>
            <input type='text' {...register('valor')} defaultValue={0} disabled={porPermilagem} />
            <span className='error-message'>{errors?.valor?.message}</span>
          </div>
          <div
            className={`ml-8 flex h-full w-1/3 ${
              errors.valor ? 'items-center' : 'items-end'
            } self-end`}
          >
            <label className='mcui-checkbox'>
              <input
                type='checkbox'
                onChange={() => {
                  setPorPermilagem(!porPermilagem);
                  setValue('valor', 0);
                }}
              />
              <div>
                <svg className='mcui-check' viewBox='-2 -2 35 35' aria-hidden='true'>
                  <title>checkmark-circle</title>
                  <polyline points='7.57 15.87 12.62 21.07 23.43 9.93' />
                </svg>
              </div>
              <div>Por permilagem</div>
            </label>
          </div>
        </div>
        <div className='md-gap-30 flex w-full justify-center'>
          <button type='submit' className='min-w-20 cursor-pointer rounded px-4 font-semibold'>
            Criar
          </button>
          <Link to={'..'} relative='path'>
            <button
              type='button'
              className='cursor-pointer rounded bg-red-100 px-4 py-2 font-semibold text-red-500'
            >
              Cancelar
            </button>
          </Link>
        </div>
      </form>
    </section>
  );
}
