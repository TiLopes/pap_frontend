import { useState, useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import axiosInstance from '@helpers/axiosInstance';
import DataTablePagination from '@components/DataTablePagination';
import DataTable from '@components/DataTable';
import { useMutation } from '@tanstack/react-query';
import ConfirmationModal from '@components/ConfirmationModal';
import Modal from 'react-modal';
import '@styles/AdminQuotas.scss';

Modal.setAppElement('#root');

async function getData(pageNo = 0) {
  const response = await axiosInstance(
    `/api/get/quotas?limit=10&offset=${pageNo * 10}&order=id&dir=ASC`,
  );

  return response;
}

function Quotas() {
  const navigate = useNavigate();
  const [showConfirmation, setShowConfirmation] = useState(false);
  const [pageData, setPageData] = useState({
    rowData: [],
    isLoading: false,
    totalPages: 0,
    totalQuotas: 0,
  });
  const [refreshData, setRefreshData] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);
  const [quotaSelected, setQuotaSelected] = useState(null);
  const columns = [
    {
      Header: 'Id',
      accessor: 'id',
      width: 100,
    },
    {
      Header: 'Fração',
      accessor: 'id_fracao',
      width: 100,
    },
    {
      Header: 'Frequência',
      accessor: 'frequencia',
      width: 200,
      Cell: ({ value }) => {
        return <span className='capitalize'>{value}</span>;
      },
    },
    {
      Header: 'Data inicio',
      accessor: 'data_inicio',
      width: 200,
    },
    {
      Header: 'Data de vencimento',
      id: 'vencimento',
      accessor: 'data_vencimento',
      width: 200,
    },
    {
      Header: 'Estado',
      accessor: 'ativa',
      width: 100,
      Cell: ({ value }) => {
        return (
          <span className={value ? 'font-bold text-green-600' : ''}>
            {value ? 'Ativa' : 'Inativa '}
          </span>
        );
      },
    },
    {
      Header: 'Valor',
      accessor: 'valor',
      width: 100,
      Cell: ({ value }) => {
        return <>{value}€</>;
      },
    },
    {
      Header: 'Ações',
      id: 'acoes',
      Cell: ({ row: { original } }) => {
        return (
          <div className='flex w-full justify-evenly'>
            <button
              className='bg-transparent p-2'
              title='Desativar'
              onClick={() => {
                setQuotaSelected(original.id);
                setShowConfirmation(true);
              }}
              disabled={!original.ativa}
            >
              <i className='bx bx-trash text-xl text-red-500'></i>
            </button>
          </div>
        );
      },
    },
  ];

  const desativarQuota = useMutation({
    mutationFn: async (id) => {
      await axiosInstance.post('/api/desativar/quota', {
        id,
      });
    },
    onSuccess: () => setRefreshData(true),
  });

  useEffect(() => {
    setPageData((prevState) => ({
      ...prevState,
      isLoading: true,
    }));

    getData(currentPage)
      .then((info) => {
        const { data } = info;
        const totalQuotas = data.count;
        const totalPages = Math.floor(totalQuotas / 10);

        setTimeout(
          () => {
            setPageData({
              isLoading: false,
              rowData: data.quotas,
              totalPages,
              totalQuotas,
            });
          },
          refreshData ? null : 250,
        );
      })
      .catch((error) => {
        console.error(error);
        if (error.response.status === 403 || error.response.status === 401) {
          Redirect();
        }
      });

    setRefreshData(false);
  }, [currentPage, refreshData]);

  function Redirect() {
    const path = '/login/condominio';
    navigate(path);
  }

  async function disableQuota() {
    desativarQuota.mutate(quotaSelected);
  }

  return (
    <section className='quotas h-full w-full'>
      <h1 className='mb-8'>Quotas</h1>
      <div className='flex gap-6'>
        <Link to='criar' className='rounded-lg bg-[#1d1b31] p-4 text-white'>
          Criar quota
        </Link>
        <Link to='pagamento' className='rounded-lg bg-[#1d1b31] p-4 text-white'>
          Pagamentos de quotas
        </Link>
      </div>
      <section className='ver-quotas mt-12'>
        <div className='max-h-fit w-full'>
          <DataTable columns={columns} data={pageData.rowData} isLoading={pageData.isLoading} />
          {pageData.totalQuotas == 0 && !pageData.isLoading && (
            <div className='mt-8 w-full'>
              <h2 className='prose mx-auto text-center text-xl'>Não existem quotas</h2>
            </div>
          )}
        </div>
        <DataTablePagination
          totalRows={pageData.totalQuotas}
          pageChangeHandler={setCurrentPage}
          rowsPerPage={10}
        />
        <ConfirmationModal
          message='Tem certeza que deseja desativar esta quota?'
          confirmModalIsOpen={showConfirmation}
          onCloseModal={() => setShowConfirmation(false)}
          onConfirmClick={disableQuota}
        />
      </section>
    </section>
  );
}

export default Quotas;
