import { Link, useNavigate } from 'react-router-dom';
import ConfirmationModal from '@components/ConfirmationModal';
import axiosInstance from '@helpers/axiosInstance';
import { ocorrenciasFilterValidation } from '@helpers/ocorrenciasFilterValidation';
import { yupResolver } from '@hookform/resolvers/yup';
import { useEffect, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import Zoom from 'react-medium-image-zoom';
import Modal from 'react-modal';
import Select from 'react-select';
import * as Yup from 'yup';
import DataTable from '@components/DataTable';
import DataTablePagination from '@components/DataTablePagination';
import { useSnackbar } from 'notistack';
import 'react-medium-image-zoom/dist/styles.css';
import '@styles/AdminOcorrencias.scss';
import { format } from 'date-fns';

Modal.setAppElement('#root');

export default function AdminOcorrencias() {
  const [selectedRow, setSelectedRow] = useState(null);
  const [modalClose, setModalClose] = useState(false);
  const [refreshData, setRefreshData] = useState(false);
  const [showFilterMenu, setShowFilterMenu] = useState(false);
  const [dataIsFiltered, setDataIsFiltered] = useState(false);
  const [dataFilters, setDataFilters] = useState(null);
  const [showConfirmation, setShowConfirmation] = useState(false);
  const [, setSolveOcorrencia] = useState(false);
  const [ocorrenciaId, setOcorrenciaId] = useState(null);
  const { enqueueSnackbar } = useSnackbar();
  const [pageData, setPageData] = useState({
    rowData: [],
    isLoading: true,
    totalPages: 0,
    totalOcorrencias: 0,
  });
  const [currentPage, setCurrentPage] = useState(0);
  const columns = [
    {
      Header: 'Id',
      accessor: 'id',
    },
    {
      Header: 'Autor',
      accessor: (row) =>
        row?.autor_condomino.nome_ocupante ? row?.autor_condomino.nome_ocupante : 'Administrador',
      maxWidth: 130,
    },
    {
      Header: 'Título',
      accessor: 'titulo',
      maxWidth: 200,
    },
    {
      Header: 'Descrição',
      accessor: 'descricao',
      maxWidth: 400,
    },
    {
      Header: 'Info. Adicional',
      accessor: 'info_adicional',
      maxWidth: 400,
    },
    {
      Header: 'Data de ocorrência',
      accessor: 'data_ocorrencia',
      minWidth: 100,
      maxWidth: 200,
    },
    {
      Header: 'Data limite de resolução',
      accessor: 'data_lim_resolucao',
      width: 250,
      canSort: true,
    },
    {
      Header: 'Estado',
      accessor: 'estado',
      width: 120,
    },
    {
      Header: 'Ações',
      accessor: 'acoes',
      Cell: (props) => (
        <div className='flex justify-evenly'>
          <button
            className='bg-transparent'
            onClick={async () => {
              try {
                const res = await getOcorrencia(props);
                setSelectedRow(res.data);
                openInfoModal();
              } catch (err) {
                console.error(err);
                if (err.response.status === 403 || err.response.status === 401) {
                  Redirect();
                }
              }
            }}
          >
            <i className='bx bx-search text-xl text-sky-400' />
          </button>
          <button
            hidden={props?.row?.original.estado === 'Resolvida' ? true : false}
            className='bg-transparent'
            onClick={function consultar() {
              setOcorrenciaId(props?.row?.original.id);
              setShowConfirmation(true);
            }}
          >
            <i className='bx bx-check text-xl text-emerald-500' />
          </button>
        </div>
      ),
      maxWidth: 150,
    },
  ];
  const navigate = useNavigate();
  const [modalInfoIsOpen, setInfoIsOpen] = useState(false);
  const [modalImprimirIsOpen, setImprimirIsOpen] = useState(false);

  function Redirect() {
    const path = '/login/condominio';
    navigate(path);
  }

  function getOcorrencia(props) {
    return axiosInstance.get(`/api/get/ocorrencia/${props?.row?.original.id}`);
  }

  const getData = async (pageNo = 0) => {
    const response = await axiosInstance(
      `/api/get/ocorrencias?limit=10&offset=${pageNo * 10}&order=id&dir=DESC`,
    );

    return response;
  };

  const getFilteredData = async (pageNo = 0, data) => {
    const response = await axiosInstance(
      `/api/filter/ocorrencias?limit=10&offset=${pageNo * 10}&order=id&dir=DESC${
        data.data_inicial ? '&data_inicial=' + format(data.data_inicial, 'yyyy-MM-dd') : ''
      }${data.data_final ? '&data_final=' + format(data.data_final, 'yyyy-MM-dd') : ''}${
        data.estado?.value ? '&estado=' + data.estado.value : ''
      }`,
    );

    return response;
  };

  // quando mudar de página "buscar" as próximas 10 ocorrências
  useEffect(() => {
    setPageData((prevState) => ({
      ...prevState,
      isLoading: true,
    }));

    if (dataIsFiltered) {
      getFilteredData(currentPage, dataFilters).then((info) => {
        const { data } = info;
        const totalOcorrencias = data.count;
        const totalPages = Math.floor(totalOcorrencias / 10);
        setPageData({
          isLoading: false,
          rowData: data.ocorrencias,
          totalPages,
          totalOcorrencias,
        });
      });
    } else {
      getData(currentPage)
        .then((info) => {
          const { data } = info;
          const totalOcorrencias = data.count;
          const totalPages = Math.floor(totalOcorrencias / 10);
          setTimeout(
            () => {
              setPageData({
                isLoading: false,
                rowData: data.ocorrencias,
                totalPages,
                totalOcorrencias,
              });
            },
            refreshData ? null : 250,
          );
        })
        .catch((err) => {
          if (err.response.status === 403 || err.response.status === 401) {
            Redirect();
          }
        });
    }

    return () => setRefreshData(false);
  }, [currentPage, refreshData]);

  useEffect(() => {
    setPageData((prevState) => ({
      ...prevState,
      isLoading: true,
    }));

    if (dataFilters && dataIsFiltered) {
      getFilteredData(currentPage, dataFilters).then((info) => {
        const { data } = info;
        const totalOcorrencias = data.count;
        const totalPages = Math.floor(totalOcorrencias / 10);
        setPageData({
          isLoading: false,
          rowData: data.ocorrencias,
          totalPages,
          totalOcorrencias,
        });
      });
    }
  }, [dataIsFiltered, dataFilters]);

  const {
    register: registerFilter,
    handleSubmit: handleSubmitFilter,
    control,
    formState: { errors: errorsFilter },
  } = useForm({
    mode: 'onSubmit',
    resolver: yupResolver(ocorrenciasFilterValidation),
  });

  const {
    register: registerImprimir,
    handleSubmit: handleSubmitImprimir,
    formState: { errors: errorsImprimir },
    reset,
  } = useForm({
    mode: 'onSubmit',
    resolver: yupResolver(
      Yup.object({
        data_de: Yup.date()
          .nullable()
          .transform((curr, orig) => (orig === '' ? null : curr))
          .max(new Date(), 'Escolha uma data até presente')
          .test('', 'Data inválida', function (value) {
            const data_ate = this.parent.data_ate ? this.parent.data_ate.getTime() : null;

            if (data_ate && value) {
              if (value.getTime() > data_ate) {
                return false;
              }
            } else {
              return false;
            }
            return true;
          }),
        data_ate: Yup.date()
          .nullable()
          .transform((curr, orig) => (orig === '' ? null : curr))
          .test('', 'Data inválida', function (value) {
            const data_de = this.parent.data_de ? this.parent.data_de.getTime() : null;

            if (data_de && value) {
              if (value.getTime() < data_de) {
                return false;
              }
            } else {
              return false;
            }

            return true;
          }),
      }),
    ),
  });

  function openInfoModal() {
    setModalClose(false);
    setInfoIsOpen(true);
  }

  function closeInfoModal() {
    setModalClose(true);
    setTimeout(() => {
      setInfoIsOpen(false);
    }, 50);
  }

  function openImprimirModal() {
    setModalClose(false);
    setImprimirIsOpen(true);
  }

  function closeImprimirModal() {
    setModalClose(true);
    setTimeout(() => {
      setImprimirIsOpen(false);
    }, 50);
    reset();
  }

  function onFilterSubmit(data) {
    // Confirmar se todos os filtros for "apagados" se sim mostrar os resultados sem filtros
    if (
      Object.values(data).every((value) => {
        if (value === null) {
          return true;
        }
        return false;
      })
    ) {
      setDataIsFiltered(false);
      setDataFilters(null);
      setRefreshData(true);
    } else {
      setDataFilters(data);
      setDataIsFiltered(true);
    }
  }

  async function onImprimirSubmit(data) {
    try {
      axiosInstance
        .get(
          `/api/imprimir/ocorrencias?data_de=${
            format(data.data_de, "yyyy-MM-dd") || ''
          }&data_ate=${format(data.data_ate, "yyyy-MM-dd") || ''}`,
          {
            responseType: 'blob',
          },
        )
        .then((res) => {
          const href = URL.createObjectURL(res.data);
          window.open(href);
        });
    } catch (err) {
      console.log(err);
    }
  }

  async function ocorrenciaSolve() {
    setSolveOcorrencia(true);
    try {
      await axiosInstance.post(`/api/solve/ocorrencia/${ocorrenciaId}`);
      enqueueSnackbar('Ocorrência foi resolvida com sucesso', { variant: 'success' });
      setRefreshData(true);
    } catch (err) {
      console.error(err);
      enqueueSnackbar('Erro ao marcar ocorrência como resolvida', { variant: 'error' });
      if (err.response.status === 403 || err.response.status === 401) {
        Redirect();
      }
    }
    setSolveOcorrencia(false);
  }

  return (
    <section className='ocorrencias'>
      <h1 className='mb-6'>Ocorrências</h1>
      <Link to='criar' className='rounded-lg bg-[#1d1b31] p-4 text-white'>
        Criar ocorrência
      </Link>

      <section className='ver-ocorrencias'>
        <div className='filter-menu'>
          <h3 className='text-lg font-bold'>Filtros</h3>
          <div
            className='open-filter flex items-center'
            onClick={(e) => {
              const arrowParent = e.target;
              arrowParent.classList.toggle('showMenu');
              setShowFilterMenu(!showFilterMenu);
            }}
          >
            <i className='bx bx-chevron-down' />
            <span className='block h-[1px] w-full border border-solid' />
          </div>
          {showFilterMenu ? (
            <div className='ocorrencias-filtros slide-in-top dark-theme flex h-fit flex-col'>
              <form className='w-1/2' onSubmit={handleSubmitFilter(onFilterSubmit)}>
                <div className='flex w-full justify-between'>
                  <fieldset className='Fieldset'>
                    <label htmlFor='data_inicial' className='Label'>
                      De
                    </label>
                    <input
                      type='date'
                      className='Input'
                      name='data_inicial'
                      {...registerFilter('data_inicial')}
                    />
                    <div className='error-message'>
                      {errorsFilter.data_inicial?.message
                        ? String(errorsFilter.data_inicial?.message)
                        : ''}
                    </div>
                  </fieldset>
                  <fieldset className='Fieldset'>
                    <label htmlFor='data_final' className='Label'>
                      Até
                    </label>
                    <input
                      type='date'
                      className='Input'
                      name='data_final'
                      {...registerFilter('data_final')}
                    />
                    <div className='error-message'>
                      {errorsFilter.data_final?.message
                        ? String(errorsFilter.data_final?.message)
                        : ''}
                    </div>
                  </fieldset>
                </div>
                <div className='flex w-full justify-between'>
                  <fieldset className='Fieldset w-full'>
                    <label htmlFor='estado' className='Label'>
                      Estado
                    </label>
                    <Controller
                      render={({ field }) => (
                        <Select
                          {...field}
                          options={[
                            { value: 'Resolvida', label: 'Resolvida' },
                            { value: 'Pendente', label: 'Pendente' },
                          ]}
                          className='w-4/6'
                          isClearable={true}
                          placeholder='Indique o estado da ocorrência'
                        />
                      )}
                      name='estado'
                      control={control}
                    />
                    <div className='error-message'>
                      {errorsFilter.estado?.message ? String(errorsFilter.estado?.message) : ''}
                    </div>
                  </fieldset>
                </div>
                <input
                  type='submit'
                  className='Button dark-theme !mx-0 !mt-0 cursor-pointer'
                  value='Filtrar'
                />
              </form>
            </div>
          ) : null}
        </div>
        <div>
          <div className='table__info mb-3 flex items-center justify-between'>
            <p>
              Total ocorrências: <span className='font-bold'>{pageData.totalOcorrencias}</span>
            </p>
            <button className='dark-theme Button !m-0' onClick={openImprimirModal}>
              Imprimir
            </button>
          </div>
          <div className='max-h-fit overflow-x-auto md:overflow-visible'>
            <DataTable columns={columns} data={pageData.rowData} isLoading={pageData.isLoading} />
            {pageData.totalOcorrencias == 0 && !pageData.isLoading && (
              <div className='mt-8 w-full'>
                <h2 className='prose mx-auto text-center text-xl'>Não existem ocorrências</h2>
              </div>
            )}
          </div>
          <DataTablePagination
            totalRows={pageData.totalOcorrencias}
            pageChangeHandler={setCurrentPage}
            rowsPerPage={10}
          />
        </div>
        <Modal
          isOpen={modalInfoIsOpen}
          onRequestClose={closeInfoModal}
          className={`Modal ${modalClose ? 'close' : ''} dark-theme`}
          overlayClassName='Overlay'
          id='modal'
        >
          <fieldset className='Fieldset'>
            <label className='Label' htmlFor='autor'>
              Autor
            </label>
            <input
              type='text'
              className='Input'
              name='autor'
              id='autor'
              disabled
              defaultValue={
                selectedRow?.ocorrencia?.autor_condomino?.nome_ocupante || 'Administrador'
              }
            />
          </fieldset>
          <div className='flex justify-between'>
            <fieldset className='Fieldset w-2/5'>
              <label className='Label' htmlFor='data_ocorrencia'>
                Data da ocorrência
              </label>
              <input
                type='text'
                className='Input'
                name='data_ocorrencia'
                id='data_ocorrencia'
                disabled
                defaultValue={selectedRow?.ocorrencia.data_ocorrencia}
              />
            </fieldset>
            <fieldset className='Fieldset w-2/5'>
              <label className='Label' htmlFor='data_lim_resolucao'>
                Data limite de resolução
              </label>
              <input
                type='text'
                className='Input'
                name='data_lim_resolucao'
                id='data_lim_resolucao'
                disabled
                defaultValue={selectedRow?.ocorrencia.data_lim_resolucao}
              />
            </fieldset>
          </div>
          <fieldset className='Fieldset'>
            <label className='Label' htmlFor='titulo'>
              Título
            </label>
            <input
              type='text'
              className='Input'
              name='titulo'
              id='titulo'
              disabled
              defaultValue={selectedRow?.ocorrencia.titulo}
            />
          </fieldset>
          <fieldset className='Fieldset'>
            <label className='Label' htmlFor='descricao'>
              Descrição
            </label>
            <textarea
              className='Input'
              name='descricao'
              id='descricao'
              disabled
              defaultValue={selectedRow?.ocorrencia.descricao}
            />
          </fieldset>
          <fieldset className='Fieldset'>
            <label className='Label' htmlFor='info_adicional'>
              Informação adicional
            </label>
            <textarea
              className='Input !h-[150px]'
              name='info_adicional'
              id='info_adicional'
              disabled
              defaultValue={selectedRow?.ocorrencia.info_adicional}
            />
          </fieldset>
          <fieldset className='Fieldset'>
            <label className='Label' htmlFor='estado'>
              Estado
            </label>
            <input
              type='text'
              className='Input !w-1/3'
              name='estado'
              id='estado'
              disabled
              defaultValue={selectedRow?.ocorrencia.estado}
            />
          </fieldset>
          <div className='ocorrencias-imagens'>
            {selectedRow?.images.map((val, index) => (
              <Zoom key={index}>
                <img src={`${axiosInstance.defaults.baseURL}/static/${val.nome}`} />
              </Zoom>
            ))}
          </div>
          <button onClick={closeInfoModal} className='Button red'>
            Fechar
          </button>
        </Modal>

        <ConfirmationModal
          message='Tem certeza que deseja marcar esta ocorrência como resolvida?'
          confirmModalIsOpen={showConfirmation}
          onCloseModal={() => setShowConfirmation(false)}
          onConfirmClick={ocorrenciaSolve}
        />

        <Modal
          isOpen={modalImprimirIsOpen}
          onRequestClose={closeImprimirModal}
          className={`Modal ${modalClose ? 'close' : ''} dark-theme`}
          overlayClassName='Overlay'
          id='modal'
        >
          <form className='w-full' onSubmit={handleSubmitImprimir(onImprimirSubmit)}>
            <div className='flex w-full justify-around'>
              <fieldset className='Fieldset'>
                <label className='Label'>De</label>
                <input
                  type='date'
                  className='Input'
                  name='data_de'
                  {...registerImprimir('data_de')}
                />
                <div className='error-message'>
                  {errorsImprimir.data_de?.message ? String(errorsImprimir.data_de?.message) : ''}
                </div>
              </fieldset>
              <fieldset className='Fieldset'>
                <label className='Label'>Até</label>
                <input
                  type='date'
                  className='Input'
                  name='data_ate'
                  {...registerImprimir('data_ate')}
                />
                <div className='error-message'>
                  {errorsImprimir.data_ate?.message ? String(errorsImprimir.data_ate?.message) : ''}
                </div>
              </fieldset>
            </div>
            <div className='flex justify-evenly'>
              <input
                type='submit'
                value='Imprimir'
                className='Button dark-theme !m-0 cursor-pointer'
              />
              <input
                type='button'
                onClick={closeImprimirModal}
                className='Button red !m-0'
                value='Fechar'
              />
            </div>
          </form>
        </Modal>
      </section>
    </section>
  );
}
