import { Controller, useForm } from 'react-hook-form';
import { useMutation, useQuery } from '@tanstack/react-query';
import { useNavigate } from 'react-router-dom';
import axiosInstance from '@helpers/axiosInstance';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';
import { useState } from 'react';
import OcorrenciaDropFiles from '@components/OcorrenciaDropFiles';
import { addMonths, format } from 'date-fns';
import { useSnackbar } from 'notistack';
import DatePicker from 'react-date-picker';
import 'react-date-picker/dist/DatePicker.css';
import 'react-calendar/dist/Calendar.css';
import '@styles/OcorrenciaCriar.scss';

export default function CriarOcorrencia() {
  const navigate = useNavigate();
  const [acceptedFiles, setAcceptedFiles] = useState([]);
  const { enqueueSnackbar } = useSnackbar();

  async function getInfo() {
    return (await axiosInstance.get('/api/create/ocorrencia')).data;
  }

  const info = useQuery({
    queryKey: ['ocorrenciaCriar'],
    queryFn: getInfo,
  });

  function Redirect() {
    navigate('/');
  }

  const validationSchema = Yup.object({
    dataOcorrencia: Yup.date()
      .typeError('Data inválida')
      .max(new Date(), 'Escolha uma data até o presente')
      .required('Preencha este campo'),
    titulo: Yup.string().required('Preencha este campo'),
    descricao: Yup.string().required('Preencha este campo'),
    dataLimite: Yup.date()
      .typeError('Data inválida')
      .test('', 'Data antes da ocorrência', function (value) {
        if (this.parent.dataOcorrencia === null || this.parent.dataOcorrencia === undefined)
          return true;

        if (value.getTime() < this.parent.dataOcorrencia.getTime()) {
          return false;
        }

        return true;
      })
      .required('Preencha este campo'),
  });

  const criarOcorrencia = useMutation({
    mutationFn: async (data) => {
      await axiosInstance.postForm('/api/create/ocorrencia', { ...data });
    },
    onSuccess: () => {
      enqueueSnackbar('Criada com sucesso', {
        variant: 'success',
      });
      navigate('..', { relative: 'path' });
    },
    onError: () => {
      enqueueSnackbar('Oops. Tente novamente', {
        variant: 'error',
      });
    },
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
    control,
  } = useForm({
    mode: 'all',
    resolver: yupResolver(validationSchema),
    defaultValues: {
      dataOcorrencia: new Date(),
      dataLimite: addMonths(new Date(), 1),
    },
  });

  const onSubmit = (data) => {
    criarOcorrencia.mutate({
      files: acceptedFiles,
      autor: data.autor,
      dataLimite: format(data.dataLimite, 'yyyy-MM-dd'),
      dataOcorrencia: format(data.dataOcorrencia, 'yyyy-MM-dd'),
      descricao: data.descricao,
      infoAdicional: data.infoAdicional,
      titulo: data.titulo,
    });
  };

  if (info.isError) {
    if (info.error.response.status === 401 || info.error.response.status === 403) {
      Redirect();
    }
    return;
  }

  if (info.isSuccess)
    return (
      <section className='ocorrencia-criar mx-[clamp(16px,3svw,32px)] w-full'>
        <h1 className='mb-4'>Criar ocorrência</h1>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className='input-wrapper'>
            <label>Registo por</label>
            <input
              type='text'
              readOnly
              defaultValue={
                info.data?.condomino ? info.data?.condomino.nome_ocupante : 'Administrador'
              }
              {...register('autor')}
            />
          </div>
          <div className='flex w-full gap-24'>
            <div className='input-wrapper'>
              <label className='required'>Data da ocorrência</label>
              <div className='relative  w-full'>
                <Controller
                  control={control}
                  name='dataOcorrencia'
                  render={({ field: { onChange, value } }) => (
                    <DatePicker
                      className={'w-48 rounded  border border-gray-300 p-1.5'}
                      onChange={onChange}
                      value={value}
                      locale={'pt-PT'}
                      calendarClassName={'min-w-120'}
                      clearIcon={null}
                      showLeadingZeros
                    />
                  )}
                />
              </div>
              <div className='error-message'>
                {errors.dataOcorrencia?.message ? String(errors.dataOcorrencia?.message) : ''}
              </div>
            </div>
            <div className='input-wrapper'>
              <label className='required'>Data limite de resolução</label>
              <div className='relative  w-full'>
                <Controller
                  control={control}
                  name='dataLimite'
                  render={({ field: { onChange, value } }) => (
                    <DatePicker
                      className={'w-48 rounded  border border-gray-300 p-1.5'}
                      onChange={onChange}
                      value={value}
                      locale={'pt-PT'}
                      calendarClassName={'min-w-120'}
                      clearIcon={null}
                      showLeadingZeros
                    />
                  )}
                />
              </div>
              <div className='error-message'>
                {errors.dataLimite?.message ? String(errors.dataLimite?.message) : ''}
              </div>
            </div>
          </div>
          <div className='input-wrapper'>
            <label className='required'>Título</label>
            <input type='text' {...register('titulo')} />
            <div className='error-message'>
              {errors.titulo?.message ? String(errors.titulo?.message) : ''}
            </div>
          </div>
          <div className='input-wrapper'>
            <label className='required'>Descrição</label>
            <textarea className='h-[10em] resize-none' {...register('descricao')} />
            <div className='error-message'>
              {errors.descricao?.message ? String(errors.descricao?.message) : ''}
            </div>
          </div>
          <div className='input-wrapper'>
            <label>Informação adicional</label>
            <textarea className='h-[10em] resize-none' {...register('infoAdicional')} />
          </div>

          <div className='input-wrapper'>
            <div className='container mt-5 text-center'>
              {/* files é uma função que altera uma variável acceptedFiles no pai*/}
              <OcorrenciaDropFiles files={(files) => setAcceptedFiles(files)} />
            </div>
          </div>
          <input type='submit' value='Criar' className='cursor-pointer !px-4 font-semibold' />
          <input
            type='button'
            onClick={() =>
              navigate('..', {
                relative: 'path',
              })
            }
            value='Cancelar'
            className='cursor-pointer !bg-red-100 !px-4 font-semibold !text-red-500'
          />
        </form>
      </section>
    );
}
