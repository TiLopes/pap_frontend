import { useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import CurrencyInput from 'react-currency-input-field';
import * as Yup from 'yup';
import { useMutation, useQuery } from '@tanstack/react-query';
import axiosInstance from '@helpers/axiosInstance';
import Select from 'react-select';
import { useSnackbar } from 'notistack';
import 'react-calendar/dist/Calendar.css';
import { useNavigate } from 'react-router-dom';
import DatePicker from 'react-date-picker';
import 'react-date-picker/dist/DatePicker.css';
import 'react-calendar/dist/Calendar.css';

const receitaSchema = Yup.object({
  valor: Yup.number()
    .typeError('Tem que ser um número')
    .min(1, 'Valor inválido')
    .required('Preencha este campo'),
  data: Yup.date().required().max(new Date(), 'Escolha uma data até o presente'),
  descritivo: Yup.string().required('Preencha este campo'),
  observacoes: Yup.string().notRequired(),
});

const Receita = () => {
  const { enqueueSnackbar } = useSnackbar();
  const navigate = useNavigate();
  const [condominosSelect, setCondominosSelect] = useState([]);

  const data = useQuery({
    queryKey: ['registar', 'receita'],
    queryFn: async () => {
      const res = await axiosInstance.get('/api/get/condominos/basic');
      return res.data;
    },
    onSuccess: (data) => {
      for (const condomino of data.condominos) {
        setCondominosSelect((prev) => [
          ...prev,
          { value: condomino.id, label: condomino.nome_ocupante },
        ]);
      }
    },
    onError: (error) => {
      console.log(error.response);
      if (error.response.status === 403 || error.response.status === 401) {
        navigate('/');
      }
    },
  });

  const registarReceita = useMutation({
    mutationFn: async (data) => {
      await axiosInstance.post('/api/add/movimento', {
        ...data,
      });
    },
    onSuccess: () => {
      enqueueSnackbar('Adicionado com sucesso', {
        variant: 'success',
        autoHideDuration: 2000,
      });
    },
  });

  const { register, formState, handleSubmit, setValue, control } = useForm({
    defaultValues: {
      valor: '0.00€',
      descritivo: '',
      observacoes: '',
      condomino: null,
    },
    resolver: yupResolver(receitaSchema),
    mode: 'all',
  });

  const onSubmit = (formData) => {
    console.log({ formData });

    registarReceita.mutate({
      descritivo: formData.descritivo,
      valor: formData.valor,
      data: formData.data,
      observacoes: formData.observacoes,
      condomino: formData.condomino.value,
      tipo: 'receita',
    });
  };

  return (
    <>
      <div className='border-rd border border-neutral-300 p-6'>
        <h2 className='prose mb-6 text-2xl font-bold'>Receita</h2>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className='grid-row-[1fr_1fr] md-grid-cols-[11rem_1fr] mb-6 grid items-center gap-2'>
            <label htmlFor='descritivo' className='required font-bold'>
              Descritivo:
            </label>
            <input
              type='text'
              {...register('descritivo')}
              maxLength={60}
              className='border-rd md-w-4/6 w-full border border-neutral-300 p-1.5'
            />
          </div>
          <div className='grid-row-[1fr_1fr] md-grid-cols-[11rem_1fr] mb-6 grid items-center gap-2'>
            <label htmlFor='valor' className='required font-bold'>
              Valor:
            </label>
            <CurrencyInput
              {...register('valor')}
              suffix='€'
              name='valor'
              defaultValue={0}
              decimalScale={2}
              onValueChange={(value) => setValue('valor', value)}
              className='border-rd w-fit border border-neutral-300 p-1.5'
            />
            <span className='prose grid-col-[1/-1] text-sm text-red-400'>
              {formState.errors.valor?.message}
            </span>
          </div>
          <div className='grid-row-[1fr_1fr] md-grid-cols-[11rem_1fr] mb-6 grid items-center gap-2'>
            <label htmlFor='data' className='required font-bold'>
              Data:
            </label>
            <div className='relative w-fit'>
              <Controller
                control={control}
                name='data'
                render={({ field: { onChange, value } }) => (
                  <DatePicker
                    className={'w-48 rounded  border border-gray-300 p-1.5'}
                    onChange={onChange}
                    value={value}
                    locale={'pt-PT'}
                    calendarClassName={'min-w-120'}
                    clearIcon={null}
                    showLeadingZeros
                    maxDate={new Date()}
                  />
                )}
              />
            </div>
            <span className='prose grid-col-[1/-1] text-sm text-red-400'>
              {formState.errors.data?.message}
            </span>
          </div>
          <div className='grid-row-[1fr_1fr] md-grid-cols-[11rem_1fr] mb-6 grid items-center gap-2'>
            <label htmlFor='condomino' className='font-bold'>
              Condómino:
            </label>
            <Controller
              name={'condomino'}
              control={control}
              render={({ field: { onChange, value } }) => (
                <Select
                  options={condominosSelect}
                  onChange={onChange}
                  isClearable
                  className='md-w-1/2 w-full'
                  value={value}
                  placeholder={'Selecione um condómino'}
                />
              )}
            />
          </div>
          <div className='grid-row-[1fr_1fr] md-grid-cols-[11rem_1fr] mb-6 grid items-center gap-2'>
            <label htmlFor='observacoes' className='font-bold'>
              Observações:
            </label>
            <textarea
              name='observacoes'
              {...register('observacoes')}
              maxLength={255}
              className='border-rd md-w-4/6 w-full border border-neutral-300 p-1.5'
            />
          </div>
          <div className='flex w-full justify-evenly'>
            <button
              type='submit'
              className='rounded bg-[#1d1b31] px-4 py-2 font-semibold text-white'
              disabled={!formState.isValid || registarReceita.isLoading}
            >
              Registar
            </button>
            <button
              type='button'
              className='rounded bg-red-100 px-4 py-2 font-semibold text-red-500'
              onClick={() => navigate('..')}
            >
              Cancelar
            </button>
          </div>
        </form>
      </div>
    </>
  );
};

export default Receita;
