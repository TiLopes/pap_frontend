import { Link, useNavigate } from 'react-router-dom';
import { useEffect, useState } from 'react';
import DataTable from '@components/DataTable';
import DataTablePagination from '@components/DataTablePagination';
import axiosInstance from '@helpers/axiosInstance';
import ReactModal from 'react-modal';
import '@styles/AdminMovimentos.scss';

ReactModal.setAppElement('#root');

function Movimentos() {
  const navigate = useNavigate();
  const [selectedMovimento, setSelectedMovimento] = useState(null);
  const [openModal, setOpenModal] = useState(false);
  const [pageData, setPageData] = useState({
    rowData: [],
    isLoading: true,
    totalPages: 0,
    totalMovimentos: 0,
  });
  const [refreshData, setRefreshData] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);
  const columns = [
    {
      Header: 'Id',
      accessor: 'id',
    },
    {
      Header: 'Tipo',
      accessor: 'tipo',
      width: 100,
    },
    {
      Header: 'Descritivo',
      accessor: 'descritivo',
      width: 200,
    },
    {
      Header: 'Data',
      accessor: 'data',
      width: 100,
    },
    {
      Header: 'Condómino',
      accessor: 'condomino',
      width: 200,
    },
    {
      Header: 'Valor',
      accessor: 'valor',
      width: 100,
      Cell: ({ value }) => <>{value}€</>,
    },
    {
      Header: 'Observações',
      accessor: 'observacoes',
      width: 250,
    },
    {
      Header: 'Ações',
      Cell: ({ row: { original } }) => {
        return (
          <>
            <div className='flex w-full items-center justify-evenly'>
              <button
                className='bg-transparent p-2'
                onClick={async () => {
                  const res = await axiosInstance.get(`/api/get/movimento/${original.id}`);
                  setSelectedMovimento(res.data.movimento);
                }}
              >
                <i className='bx bx-search text-xl !font-bold text-sky-400'></i>
              </button>
            </div>
          </>
        );
      },
      width: 100,
    },
  ];

  const getData = async (pageNo = 0) => {
    const response = await axiosInstance(
      `/api/get/movimentos?limit=10&offset=${pageNo * 10}&order=id&dir=ASC`,
    );

    return response;
  };

  useEffect(() => {
    setPageData((prevState) => ({
      ...prevState,
      isLoading: true,
    }));

    getData(currentPage)
      .then((info) => {
        const { data } = info;
        const totalMovimentos = data.count;
        const totalPages = Math.floor(totalMovimentos / 10);
        setTimeout(
          () => {
            setPageData({
              isLoading: false,
              rowData: data.movimentos,
              totalPages,
              totalMovimentos,
            });
          },
          refreshData ? null : 100,
        );
      })
      .catch((err) => {
        if (err.response.status === 403 || err.response.status === 401) {
          navigate('/login/condominio');
        }
      });

    return () => setRefreshData(false);
  }, [currentPage, refreshData]);

  useEffect(
    () => {
      // apenas executar quando movimentoId estiver definido
      if (selectedMovimento) {
        setOpenModal(true);
      }
    },
    [selectedMovimento] /* lista de dependências */,
  );

  return (
    <>
      <section className='movimentos w-full'>
        <h1 className='prose mb-8 mt-4 !text-4xl'>Movimentos</h1>
        <Link to='registar' className='rounded-lg bg-[#1d1b31] p-4 text-white'>
          Registar movimento
        </Link>
        <section className='ver-movimentos mt-12 w-full'>
          <div className='md-overflow-x-visible max-h-fit w-full overflow-x-auto'>
            <DataTable columns={columns} data={pageData.rowData} isLoading={pageData.isLoading} />
            {pageData.totalMovimentos == 0 && !pageData.isLoading && (
              <div className='mt-8 w-full'>
                <h2 className='prose mx-auto text-center text-xl'>Não existem movimentos</h2>
              </div>
            )}
          </div>
          <DataTablePagination
            totalRows={pageData.totalMovimentos}
            pageChangeHandler={setCurrentPage}
            rowsPerPage={10}
          />
        </section>
        <ReactModal
          overlayClassName='Overlay'
          onRequestClose={() => {
            setOpenModal(false);
            setSelectedMovimento(null);
          }}
          className={`Modal ${!openModal ? 'close' : ''} dark-theme`}
          closeTimeoutMS={100}
          id='modal'
          shouldCloseOnEsc
          shouldCloseOnOverlayClick
          isOpen={openModal}
        >
          <fieldset className='mb-6 flex flex-col'>
            <label className='prose text-md mb-2 font-bold text-violet-800'>Nº movimento:</label>
            <input
              disabled
              type='text'
              defaultValue={selectedMovimento?.id}
              className='bordered border-rd w-fit border border-neutral-200 bg-zinc-100 p-2'
            />
          </fieldset>
          <fieldset className='mb-6 flex flex-col'>
            <label className='prose text-md mb-2 font-bold text-violet-800'>Tipo</label>
            <input
              disabled
              type='text'
              defaultValue={selectedMovimento?.tipo}
              className='bordered border-rd w-fit border border-neutral-200 bg-zinc-100 p-2'
            />
          </fieldset>
          <fieldset className='mb-6 flex flex-col'>
            <label className='prose text-md mb-2 font-bold text-violet-800'>Descritivo</label>
            <input
              disabled
              type='text'
              defaultValue={selectedMovimento?.descritivo}
              className='bordered border-rd w-full border border-neutral-200 bg-zinc-100 p-2'
            />
          </fieldset>
          <fieldset className='mb-6 flex flex-col'>
            <label className='prose text-md mb-2 font-bold text-violet-800'>
              Data do pagamento
            </label>
            <input
              disabled
              type='text'
              defaultValue={selectedMovimento?.data}
              className='bordered border-rd w-fit border border-neutral-200 bg-zinc-100 p-2'
            />
          </fieldset>
          <fieldset className='mb-6 flex flex-col'>
            <label className='prose text-md mb-2 font-bold text-violet-800'>Condómino</label>
            <input
              disabled
              type='text'
              defaultValue={selectedMovimento?.condomino}
              className='bordered border-rd w-full border border-neutral-200 bg-zinc-100 p-2'
            />
          </fieldset>
          <fieldset className='mb-6 flex flex-col'>
            <label className='prose text-md mb-2 font-bold text-violet-800'>Tipo</label>
            <input
              disabled
              type='text'
              defaultValue={selectedMovimento?.tipo}
              className='bordered border-rd w-fit border border-neutral-200 bg-zinc-100 p-2'
            />
          </fieldset>
          <fieldset className='mb-6 flex flex-col'>
            <label className='prose text-md mb-2 font-bold text-violet-800'>Valor</label>
            <input
              disabled
              type='text'
              defaultValue={selectedMovimento?.valor + '€'}
              className='bordered border-rd w-fit border border-neutral-200 bg-zinc-100 p-2'
            />
          </fieldset>
          <fieldset className='mb-6 flex flex-col'>
            <label className='prose text-md mb-2 font-bold text-violet-800'>Observações</label>
            <textarea
              disabled
              type='text'
              defaultValue={selectedMovimento?.observacoes}
              className='bordered border-rd w-full border border-neutral-200 bg-zinc-100 p-2'
            />
          </fieldset>
          <div className='sticky bottom-0 flex w-full justify-center'>
            <button
              className='box-shadow mx-auto rounded bg-red-100 px-4 py-2 font-semibold text-red-500'
              onClick={() => setOpenModal(false)}
            >
              Fechar
            </button>
          </div>
        </ReactModal>
      </section>
    </>
  );
}

export default Movimentos;
