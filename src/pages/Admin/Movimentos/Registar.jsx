import Select from 'react-select';
import '@styles/AdminMovimentos.scss';
import { Outlet, useNavigate } from 'react-router-dom';

function Registar() {
  const navigate = useNavigate();

  return (
    <section className='movimento-criar'>
      <div className='flex items-center gap-6 mt-4'>
        <button className='bg-transparent' onClick={() => navigate('..')}>
          <i className='bx bx-arrow-back prose text-4xl align-sub'></i>
        </button>
        <h1 className='prose text-4xl'>Registar movimento</h1>
      </div>
      <Select
        options={[
          { value: 'despesa', label: 'Despesa' },
          { value: 'receita', label: 'Receita' },
        ]}
        onChange={(e) => navigate(e.value)}
        className='md-w-96 my-6'
				placeholder={'Selecione o tipo de movimento'}
      />
      <Outlet />
    </section>
  );
}

export default Registar;
