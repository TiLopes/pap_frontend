import { Link } from 'react-router-dom';
import '@styles/Frontpage.scss';
import { Popover, Transition } from '@headlessui/react';
import { useAuth } from '@context/Auth.context';
import { useState } from 'react';

function Frontpage() {
  const { isLoggedIn } = useAuth();
  const [group] = useState(localStorage.getItem('group'));

  return (
    <>
      <nav
        className={
          'flex-between relative top-0 flex justify-between bg-[#1d1b31] px-8 py-6 text-white'
        }
      >
        <img src={'/logo.svg'} width={48} />
        <ul className={'flex gap-10'}>
          {isLoggedIn() ? (
            <li className={'flex items-center gap-2 font-semibold'}>
              <i className={'bx bx-buildings text-2xl'}></i>
              <Link to={group == 999 ? 'administracao' : 'condominos'}>Área de serviço</Link>
            </li>
          ) : (
            <>
              <li className={'flex items-center font-semibold'}>
                <Link to={'signup/condominio'}>Registar condomínio</Link>
              </li>
              <li className={'flex items-center'}>
                <Popover className='relative'>
                  {({ open }) => (
                    <>
                      <Popover.Button
                        className={
                          'prose flex items-center bg-transparent p-2 font-semibold outline-transparent'
                        }
                      >
                        Entrar
                        <i
                          className={`bx bx-chevron-down ease transition duration-100 ${
                            open ? 'rotate-180 transform' : ''
                          }`}
                        ></i>
                      </Popover.Button>
                      <Transition
                        enter='transition duration-100 ease-out'
                        enterFrom='transform scale-95 opacity-0'
                        enterTo='transform scale-100 opacity-100'
                        leave='transition duration-75 ease-out'
                        leaveFrom='transform scale-100 opacity-100'
                        leaveTo='transform scale-95 opacity-0'
                      >
                        <Popover.Panel className='w-50 absolute right-0 z-10 transform rounded bg-white p-4 text-black shadow-lg'>
                          <div className='grid grid-cols-1 gap-2 text-center'>
                            <Link to='/login/condominio' className={'font-normal'}>
                              Como condomínio
                            </Link>
                            <Link to='/login' className={'font-normal'}>
                              Como condómino
                            </Link>
                          </div>
                        </Popover.Panel>
                      </Transition>
                    </>
                  )}
                </Popover>
              </li>
            </>
          )}
        </ul>
      </nav>
      <section className={'background-image flex h-[calc(100%_-_96px)] w-full flex-col'}>
        <div className='h-full w-full px-16 pt-8 text-white'>
          <h1 className={'text-6xl font-bold'}>Comece já a gerir o seu condomínio!</h1>
          <div className={'mt-4 flex flex-col gap-4 w-full'}>
            <p className={'prose text-2xl '}>Se já tem conta clique no botão &quot;Entrar&quot;.</p>
            <p className={'prose text-2xl'}>
              É um condomínio e ainda não tem conta, clique em &quot;Registar condomínio&quot; e
              crie uma conta completamente <strong>gratuíta</strong>.
            </p>
          </div>
        </div>
      </section>
    </>
  );
}

export default Frontpage;
