import axiosInstance from '@helpers/axiosInstance';
import { useQuery } from '@tanstack/react-query';
import { useNavigate } from 'react-router-dom';
import { useAuth } from '@context/Auth.context';

const Perfil = () => {
  const navigate = useNavigate();

  // fazer um pedido a API
  const info = useQuery({
    // chave única do pedido
    queryKey: ['condomino', 'perfil'],
    // função que faz pedido
    queryFn: async () => {
      const res = await axiosInstance.get('/api/get/me');
      // devolver a informação
      return res.data.me;
    },
    // se der erro
    onError: (err) => {
      // se não tiver permissão redirecionar para a página inicial
      if (err.response.status === 401 || err.response.status === 403) {
        navigate('/');
      }
    },
  });

  // redirecionar para o login se o utilizador não estiver autenticado
  // if (isLoggedOut()) {
  //   navigate('/login/condominio');
  //   logout();
  // }

  // renderizar a página se o pedido foi feito com sucesso
  if (info.isSuccess)
    return (
      <section className='mx-[clamp(16px,3svw,32px)] w-full'>
        <h1 className='prose mt-4 mb-6 text-4xl font-bold'>Perfil</h1>
        <div className='md-w-2xl border-rd-md min-h-2xl w-full border p-8'>
          <fieldset className='mb-6 flex w-full flex-col'>
            <label className='prose text-md mb-2 font-bold'>Nome</label>
            <input
              disabled
              type='text'
              className='bordered border-rd w-full border border-neutral-200 bg-zinc-100 p-2'
              defaultValue={info.data.nome_ocupante}
            />
          </fieldset>
          <div className='md-flex-nowrap flex flex-wrap justify-between'>
            <fieldset className='md-w-fit mb-6 flex w-full flex-col'>
              <label className='prose text-md mb-2 font-bold'>NIF</label>
              <input
                disabled
                type='text'
                className='bordered border-rd md-w-fit w-full border border-neutral-200 bg-zinc-100 p-2'
                defaultValue={info.data.nif_ocupante}
              />
            </fieldset>
            <fieldset className='md-w-fit mb-6 flex w-full flex-col'>
              <label className='prose text-md mb-2 font-bold'>Nº telemóvel</label>
              <input
                disabled
                type='text'
                className='bordered border-rd md-w-fit w-full border border-neutral-200 bg-zinc-100 p-2'
                defaultValue={info.data.telemovel_ocupante}
              />
            </fieldset>
          </div>
          <fieldset className='mb-6 flex w-full flex-col'>
            <label className='prose text-md mb-2 font-bold'>Email</label>
            <input
              disabled
              type='text'
              className='bordered border-rd w-full border border-neutral-200 bg-zinc-100 p-2'
              defaultValue={info.data.email_ocupante}
            />
          </fieldset>
          <fieldset className='mb-6 flex w-full flex-col'>
            <label className='prose text-md mb-2 font-bold'>Nome de utilizador</label>
            <input
              disabled
              type='text'
              className='bordered border-rd md-w-fit w-full border border-neutral-200 bg-zinc-100 p-2'
              defaultValue={info.data.username}
            />
          </fieldset>
          <fieldset className='mb-6 flex w-full flex-col'>
            <label className='prose text-md mb-2 font-bold'>Nome do condomínio</label>
            <input
              disabled
              type='text'
              className='bordered border-rd w-full border border-neutral-200 bg-zinc-100 p-2'
              defaultValue={info.data.condominio.nome}
            />
          </fieldset>
          <div className='md-flex-nowrap flex flex-wrap justify-between gap-8'>
            <fieldset className='md-w-fit mb-6 flex w-full flex-col'>
              <label className='prose text-md mb-2 font-bold'>Morada</label>
              <input
                disabled
                type='text'
                className='bordered border-rd md-w-sm w-full border border-neutral-200 bg-zinc-100 p-2'
                defaultValue={info.data.condominio.morada}
              />
            </fieldset>
            <fieldset className='md-max-w-sm mb-6 flex w-full flex-col'>
              <label className='prose text-md mb-2 font-bold'>Código postal</label>
              <input
                disabled
                type='text'
                className='bordered border-rd w-full border border-neutral-200 bg-zinc-100 p-2'
                defaultValue={info.data.condominio.cod_postal}
              />
            </fieldset>
          </div>
        </div>
      </section>
    );
};

export default Perfil;
