import Navbar from '@components/Navbar';
import { useAuth } from '@context/Auth.context';
import { SnackbarProvider } from 'notistack';
import { Helmet, HelmetProvider } from 'react-helmet-async';
import { Outlet } from 'react-router-dom';
import '@styles/Condomino.scss';

const index = () => {
  const { isLoggedIn } = useAuth();

  if (isLoggedIn()) {
    return (
      <SnackbarProvider
        maxSnack={3}
        anchorOrigin={{
          horizontal: 'right',
          vertical: 'top',
        }}
      >
        <Navbar />
        <section className='home-section' id='home-section'>
          <div className='home-content h-full w-full'>
            <Outlet />
          </div>
        </section>
      </SnackbarProvider>
    );
  }
};

export default index;
