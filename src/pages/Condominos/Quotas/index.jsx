import { useState, useEffect } from 'react';
import { Link, useNavigate, Navigate } from 'react-router-dom';
import { useAuth } from '@context/Auth.context';
import axiosInstance from '@helpers/axiosInstance';
import DataTablePagination from '@components/DataTablePagination';
import DataTable from '@components/DataTable';
import { useForm } from 'react-hook-form';
import { useMutation } from '@tanstack/react-query';
import '@styles/AdminQuotas.scss';

const Quotas = () => {
  const navigate = useNavigate();
  const { isLoggedIn, isLoggedOut } = useAuth();
  const [pageData, setPageData] = useState({
    rowData: [],
    isLoading: false,
    totalPages: 0,
    totalQuotas: 0,
  });
  const [refreshData, setRefreshData] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);
  const columns = [
    {
      Header: 'Id',
      accessor: 'id',
      width: 100,
    },
    {
      Header: 'Fração',
      accessor: 'id_fracao',
      width: 100,
    },
    {
      Header: 'Frequência',
      accessor: 'frequencia',
      width: 200,
      Cell: ({ value }) => {
        return <span className='capitalize'>{value}</span>;
      },
    },
    {
      Header: 'Data inicio',
      accessor: 'data_inicio',
      width: 200,
    },
    {
      Header: 'Data de vencimento',
      id: 'vencimento',
      accessor: 'data_vencimento',
      width: 200,
    },
    {
      Header: 'Estado',
      accessor: 'ativa',
      width: 100,
      Cell: ({ value }) => {
        return (
          <span className={value ? 'font-bold text-green-600' : ''}>
            {value ? 'Ativa' : 'Inativa '}
          </span>
        );
      },
    },
    {
      Header: 'Valor',
      accessor: 'valor',
      width: 100,
      Cell: ({ value }) => {
        return <>{value}€</>;
      },
    },
  ];

  useEffect(() => {
    setPageData((prevState) => ({
      ...prevState,
      rowData: [],
      isLoading: true,
    }));

    getData(currentPage)
      .then((info) => {
        const { data } = info;
        const totalQuotas = data.count;
        const totalPages = Math.floor(totalQuotas / 10);
        setTimeout(() => {
          setPageData({
            isLoading: false,
            rowData: data.quotas,
            totalPages,
            totalQuotas,
          });
        }, 100);
      })
      .catch((err) => {
        if (err.response.status === 403 || err.response.status === 401) {
          Redirect();
        }
      });
  }, [currentPage]);

  useEffect(() => {
    setPageData((prevState) => ({
      ...prevState,
      rowData: [],
      isLoading: true,
    }));

    getData(currentPage)
      .then((info) => {
        const { data } = info;
        const totalQuotas = data.count;
        const totalPages = Math.floor(totalQuotas / 10);

        setPageData({
          isLoading: false,
          rowData: data.quotas,
          totalPages,
          totalQuotas,
        });
      })
      .catch((err) => {
        if (err.response.status === 403 || err.response.status === 401) {
          Redirect();
        }
      });

    return () => setRefreshData(false);
  }, [refreshData]);

  async function getData(pageNo = 0) {
    const response = await axiosInstance(
      `/api/my/quotas?limit=10&offset=${pageNo * 10}&order=id&dir=ASC`,
    );

    return response;
  }

  function Redirect() {
    const path = '/login/condominio';
    navigate(path);
  }

  if (isLoggedOut()) {
    return <Navigate to={'/login/condominio'} />;
  }

  return (
    <section className='mx-[clamp(16px,3svw,32px)] w-full'>
      <h1 className='prose mb-8 text-4xl mt-4 font-bold'>Quotas</h1>
      <div className='flex gap-6'>
        <Link to='pagamentos' className='rounded-lg bg-[#1d1b31] p-4 text-white'>
          Pagamentos de quotas
        </Link>
      </div>
      <section className='ver-quotas mt-12'>
        <div className='max-h-fit'>
          <DataTable columns={columns} data={pageData.rowData} isLoading={pageData.isLoading} />
          {pageData.totalQuotas == 0 && !pageData.isLoading && (
            <div className='mt-8 w-full'>
              <h2 className='prose mx-auto text-center text-xl'>Não existem quotas</h2>
            </div>
          )}
        </div>
        <DataTablePagination
          totalRows={pageData.totalQuotas}
          pageChangeHandler={setCurrentPage}
          rowsPerPage={10}
        />
      </section>
    </section>
  );
};

export default Quotas;
