import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import axiosInstance from '@helpers/axiosInstance';
import DataTablePagination from '@components/DataTablePagination';
import DataTable from '@components/DataTable';
import { format } from 'date-fns';
import pt from 'date-fns/locale/pt';

const Pagamentos = () => {
  const navigate = useNavigate();
  const [pageData, setPageData] = useState({
    rowData: [],
    isLoading: false,
    totalPages: 0,
    totalPagamentosQuotas: 0,
  });
  const [currentPage, setCurrentPage] = useState(0);
  const columns = [
    {
      Header: 'Fração',
      accessor: 'fracao',
      width: 100,
    },
    {
      Header: 'Data',
      accessor: 'data',
      width: 200,
    },
    {
      Header: 'Período',
      accessor: 'periodo',
      width: 200,
      Cell: ({ value }) => <>{format(new Date(value), "MMMM 'de' yyyy", { locale: pt })}</>,
    },
    {
      Header: 'Valor',
      accessor: 'valor',
      width: 150,
      Cell: ({ value }) => <>{value}€</>,
    },
    {
      Header: 'Observações',
      accessor: 'observacoes',
      width: 200,
    },
  ];

  const getData = async (pageNo = 0) => {
    const response = await axiosInstance(
      `/api/my/pagamentos_quotas?limit=10&offset=${pageNo * 10}&order=id&dir=ASC`,
    );

    return response;
  };

  useEffect(() => {
    setPageData((prevState) => ({
      ...prevState,
      isLoading: true,
    }));

    getData(currentPage)
      .then((info) => {
        const { data } = info;
        const totalPagamentosQuotas = data.count;
        const totalPages = Math.floor(totalPagamentosQuotas / 10);
        setTimeout(() => {
          setPageData({
            isLoading: false,
            rowData: data.pagamentos_quotas,
            totalPages,
            totalPagamentosQuotas,
          });
        }, 500);
      })
      .catch((err) => {
        if (err.response.status === 403 || err.response.status === 401) {
          navigate('/');
        }
      });
  }, [currentPage]);

  return (
    <section className='mx-[clamp(16px,3svw,32px)] w-full'>
      <div className='mb-10 mt-4 flex items-center gap-6'>
        <button className='bg-transparent' onClick={() => navigate('..')}>
          <i className='bx bx-arrow-back prose align-sub text-4xl'></i>
        </button>
        <h1 className='prose '>Pagamento quotas</h1>
      </div>
      <div className='mt-8 max-h-fit w-full'>
        <DataTable columns={columns} data={pageData.rowData} isLoading={pageData.isLoading} />
        {pageData.totalPagamentosQuotas == 0 && !pageData.isLoading && (
          <div className='mt-8 w-full'>
            <h2 className='prose mx-auto text-center text-xl'>Não existem pagamentos</h2>
          </div>
        )}
      </div>
      <DataTablePagination
        totalRows={pageData.totalPagamentosQuotas}
        pageChangeHandler={setCurrentPage}
        rowsPerPage={10}
      />
    </section>
  );
};

export default Pagamentos;
