import axiosInstance from '@helpers/axiosInstance';
import { useQuery } from '@tanstack/react-query';
import { useNavigate } from 'react-router-dom';

const index = () => {
  const navigate = useNavigate();
  const condominioInfo = useQuery({
    queryKey: ['condominos'],
    queryFn: () => {
      return axiosInstance.get('/api/get/me');
    },
    onSuccess: ({ data }) => {
      console.log(data);
    },
    onError: () => {
      navigate('/');
    },
  });

  if (condominioInfo.isSuccess)
    return (
      <section className={'mx-[clamp(16px,3svw,32px)]'}>
        <h1 className={'prose'}>Bem vindo {condominioInfo.data.data.me.nome_ocupante}!</h1>
        <p className={'prose'}>
          Utilize a barra de navegação no lado esquerdo para navegar na nossa plataforma.
        </p>
      </section>
    );
};

export default index;
