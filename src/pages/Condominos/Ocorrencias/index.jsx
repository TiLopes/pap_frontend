import { Link, useNavigate } from 'react-router-dom';
import axiosInstance from '@helpers/axiosInstance';
import { ocorrenciasFilterValidation } from '@helpers/ocorrenciasFilterValidation';
import { yupResolver } from '@hookform/resolvers/yup';
import { useEffect, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import Zoom from 'react-medium-image-zoom';
import Modal from 'react-modal';
import Select from 'react-select';
import DataTable from '@components/DataTable';
import DataTablePagination from '@components/DataTablePagination';
import { useAuth } from '@context/Auth.context';
import { format } from 'date-fns';
import { useMutation } from '@tanstack/react-query';
import 'react-medium-image-zoom/dist/styles.css';
import '@styles/AdminOcorrencias.scss';

Modal.setAppElement('#root');

const Ocorrencias = () => {
  const [selectedRow, setSelectedRow] = useState(null);
  const [modalClose, setModalClose] = useState(false);
  const [refreshData, setRefreshData] = useState(false);
  const [showFilterMenu, setShowFilterMenu] = useState(false);
  const [dataIsFiltered, setDataIsFiltered] = useState(false);
  const [dataFilters, setDataFilters] = useState(null);
  const [pageData, setPageData] = useState({
    rowData: [],
    isLoading: false,
    totalPages: 0,
    totalOcorrencias: 0,
  });
  const [currentPage, setCurrentPage] = useState(0);
  const columns = [
    {
      Header: 'Id',
      accessor: 'id',
    },
    {
      Header: 'Autor',
      accessor: (row) =>
        row?.autor_condomino.nome_ocupante ? row?.autor_condomino.nome_ocupante : 'Administrador',
      maxWidth: 130,
    },
    {
      Header: 'Título',
      accessor: 'titulo',
      maxWidth: 200,
    },
    {
      Header: 'Descrição',
      accessor: 'descricao',
      maxWidth: 400,
    },
    {
      Header: 'Info. Adicional',
      accessor: 'info_adicional',
      maxWidth: 400,
    },
    {
      Header: 'Data de ocorrência',
      accessor: 'data_ocorrencia',
      minWidth: 100,
      maxWidth: 200,
    },
    {
      Header: 'Data limite de resolução',
      accessor: 'data_lim_resolucao',
      width: 250,
      canSort: true,
    },
    {
      Header: 'Estado',
      accessor: 'estado',
      width: 120,
    },
    {
      Header: 'Ações',
      accessor: 'acoes',
      Cell: ({ row: { original } }) => (
        <div className='flex justify-evenly'>
          <button className='bg-transparent' onClick={() => verQuota.mutate(original.id)}>
            <i className='bx bx-search text-xl !font-bold text-sky-400' />
          </button>
        </div>
      ),
      maxWidth: 150,
    },
  ];
  const navigate = useNavigate();
  const [modalInfoIsOpen, setInfoIsOpen] = useState(false);
  const { isLoggedOut, logout } = useAuth();
  const verQuota = useMutation({
    mutationFn: async (id) => {
      return await axiosInstance.get(`/api/get/ocorrencia/${id}`);
    },
    onSuccess: ({ data }) => {
      setSelectedRow(data);
      openInfoModal();
    },
    onError: (err) => {
      console.log(err.response);
      if (err.response.status === 403 || err.response.status === 401) {
        Redirect();
      }
    },
  });

  function Redirect() {
    const path = '/login/condominio';
    navigate(path);
  }

  const getData = async (pageNo = 0) => {
    const response = await axiosInstance(
      `/api/get/ocorrencias?limit=10&offset=${pageNo * 10}&order=id&dir=DESC`,
    );

    return response;
  };

  const getFilteredData = async (pageNo = 0, data) => {
    const response = await axiosInstance(
      `/api/filter/ocorrencias?limit=10&offset=${pageNo * 10}&order=id&dir=DESC${
        data.data_inicial ? '&data_inicial=' + format(data.data_inicial, 'yyyy-MM-dd') : ''
      }${data.data_final ? '&data_final=' + format(data.data_final, 'yyyy-MM-dd') : ''}${
        data.estado?.value ? '&estado=' + data.estado.value : ''
      }`,
    );

    return response;
  };

  // quando mudar de página "buscar" as próximas 10 ocorrências
  useEffect(() => {
    setPageData((prevState) => ({
      ...prevState,
      isLoading: true,
    }));

    if (dataIsFiltered) {
      getFilteredData(currentPage, dataFilters).then((info) => {
        const { data } = info;
        const totalOcorrencias = data.count;
        const totalPages = Math.floor(totalOcorrencias / 10);
        setPageData({
          isLoading: false,
          rowData: data.ocorrencias,
          totalPages,
          totalOcorrencias,
        });
      });
    } else {
      getData(currentPage)
        .then((info) => {
          const { data } = info;
          const totalOcorrencias = data.count;
          const totalPages = Math.floor(totalOcorrencias / 10);
          setTimeout(
            () => {
              setPageData({
                isLoading: false,
                rowData: data.ocorrencias,
                totalPages,
                totalOcorrencias,
              });
            },
            refreshData ? null : 250,
          );
        })
        .catch((err) => {
          if (err.response.status === 403 || err.response.status === 401) {
            Redirect();
          }
        });
    }
  }, [currentPage]);

  useEffect(() => {
    setPageData((prevState) => ({
      ...prevState,
      isLoading: true,
    }));

    if (dataFilters && dataIsFiltered) {
      getFilteredData(currentPage, dataFilters).then((info) => {
        const { data } = info;
        const totalOcorrencias = data.count;
        const totalPages = Math.floor(totalOcorrencias / 10);
        console.log(data.ocorrencias);
        setPageData({
          isLoading: false,
          rowData: data.ocorrencias,
          totalPages,
          totalOcorrencias,
        });
      });
    }
  }, [dataIsFiltered, dataFilters]);

  const {
    register: registerFilter,
    handleSubmit: handleSubmitFilter,
    control,
    formState: { errors: errorsFilter },
  } = useForm({
    mode: 'onSubmit',
    resolver: yupResolver(ocorrenciasFilterValidation),
  });

  function openInfoModal() {
    setModalClose(false);
    setInfoIsOpen(true);
  }

  function closeInfoModal() {
    setModalClose(true);
    setTimeout(() => {
      setInfoIsOpen(false);
    }, 50);
  }

  function onFilterSubmit(data) {
    // Confirmar se todos os filtros for "apagados" se sim mostrar os resultados sem qualquer filtragem de dados
    if (
      Object.values(data).every((value) => {
        if (value === null) {
          return true;
        }
        return false;
      })
    ) {
      setDataIsFiltered(false);
      setDataFilters(null);
      setRefreshData(true);
    } else {
      setDataFilters(data);
      setDataIsFiltered(true);
    }
  }

  if (isLoggedOut()) {
    logout();
    Redirect();
    return;
  }
  return (
    <section className='ocorrencias mx-[clamp(16px,3svw,32px)] w-full'>
      <h1 className='prose mb-8 mt-4 text-4xl font-bold'>Ocorrências</h1>
      <Link to='criar' className='rounded-lg bg-[#1d1b31] p-4 text-white'>
        Criar ocorrência
      </Link>

      <section className='ver-ocorrencias'>
        <div className='filter-menu'>
          <h3 className='text-lg font-bold'>Filtros</h3>
          <div
            className='open-filter flex items-center'
            onClick={(e) => {
              const arrowParent = e.target;
              arrowParent.classList.toggle('showMenu');
              setShowFilterMenu(!showFilterMenu);
            }}
          >
            <i className='bx bx-chevron-down' />
            <span className='block h-[1px] w-full border border-solid' />
          </div>
          {showFilterMenu ? (
            <div className='ocorrencias-filtros slide-in-top dark-theme flex h-fit flex-col'>
              <form className='w-1/2' onSubmit={handleSubmitFilter(onFilterSubmit)}>
                <div className='flex w-full justify-between'>
                  <fieldset className='Fieldset'>
                    <label htmlFor='data_inicial' className='Label'>
                      De
                    </label>
                    <input
                      type='date'
                      className='Input'
                      name='data_inicial'
                      {...registerFilter('data_inicial')}
                    />
                    <div className='error-message'>
                      {errorsFilter.data_inicial?.message
                        ? String(errorsFilter.data_inicial?.message)
                        : ''}
                    </div>
                  </fieldset>
                  <fieldset className='Fieldset'>
                    <label htmlFor='data_final' className='Label'>
                      Até
                    </label>
                    <input
                      type='date'
                      className='Input'
                      name='data_final'
                      {...registerFilter('data_final')}
                    />
                    <div className='error-message'>
                      {errorsFilter.data_final?.message
                        ? String(errorsFilter.data_final?.message)
                        : ''}
                    </div>
                  </fieldset>
                </div>
                <div className='flex w-full justify-between'>
                  <fieldset className='Fieldset w-full'>
                    <label htmlFor='estado' className='Label'>
                      Estado
                    </label>
                    <Controller
                      render={({ field }) => (
                        <Select
                          {...field}
                          options={[
                            { value: 'Resolvida', label: 'Resolvida' },
                            { value: 'Pendente', label: 'Pendente' },
                          ]}
                          className='w-4/6'
                          isClearable={true}
                          placeholder='Indique o estado da ocorrência'
                        />
                      )}
                      name='estado'
                      control={control}
                    />
                    <div className='error-message'>
                      {errorsFilter.estado?.message ? String(errorsFilter.estado?.message) : ''}
                    </div>
                  </fieldset>
                </div>
                <input
                  type='submit'
                  className='Button dark-theme !mx-0 !mt-0 cursor-pointer'
                  value='Filtrar'
                />
              </form>
            </div>
          ) : null}
        </div>
        <div>
          <div className='table__info mb-3 flex items-center justify-between'>
            <p>
              Total ocorrências: <span className='font-bold'>{pageData.totalOcorrencias}</span>
            </p>
          </div>
          <div className='max-h-fit overflow-x-auto md:overflow-visible'>
            <DataTable columns={columns} data={pageData.rowData} isLoading={pageData.isLoading} />
            {pageData.totalOcorrencias == 0 && !pageData.isLoading && (
              <div className='mt-8 w-full'>
                <h2 className='prose mx-auto text-center text-xl'>Não existem ocorrências</h2>
              </div>
            )}
          </div>
          <DataTablePagination
            totalRows={pageData.totalOcorrencias}
            pageChangeHandler={setCurrentPage}
            rowsPerPage={10}
          />
        </div>
        <Modal
          isOpen={modalInfoIsOpen}
          onRequestClose={closeInfoModal}
          className={`Modal ${modalClose ? 'close' : ''} dark-theme`}
          overlayClassName='Overlay'
          id='modal'
        >
          <fieldset className='Fieldset'>
            <label className='Label' htmlFor='autor'>
              Autor
            </label>
            <input
              type='text'
              className='Input'
              name='autor'
              id='autor'
              disabled
              defaultValue={
                selectedRow?.ocorrencia?.autor_condomino?.nome_ocupante || 'Administrador'
              }
            />
          </fieldset>
          <div className='flex justify-between'>
            <fieldset className='Fieldset w-2/5'>
              <label className='Label' htmlFor='data_ocorrencia'>
                Data da ocorrência
              </label>
              <input
                type='text'
                className='Input'
                name='data_ocorrencia'
                id='data_ocorrencia'
                disabled
                defaultValue={selectedRow?.ocorrencia.data_ocorrencia}
              />
            </fieldset>
            <fieldset className='Fieldset w-2/5'>
              <label className='Label' htmlFor='data_lim_resolucao'>
                Data limite de resolução
              </label>
              <input
                type='text'
                className='Input'
                name='data_lim_resolucao'
                id='data_lim_resolucao'
                disabled
                defaultValue={selectedRow?.ocorrencia.data_lim_resolucao}
              />
            </fieldset>
          </div>
          <fieldset className='Fieldset'>
            <label className='Label' htmlFor='titulo'>
              Título
            </label>
            <input
              type='text'
              className='Input'
              name='titulo'
              id='titulo'
              disabled
              defaultValue={selectedRow?.ocorrencia.titulo}
            />
          </fieldset>
          <fieldset className='Fieldset'>
            <label className='Label' htmlFor='descricao'>
              Descrição
            </label>
            <textarea
              className='Input'
              name='descricao'
              id='descricao'
              disabled
              defaultValue={selectedRow?.ocorrencia.descricao}
            />
          </fieldset>
          <fieldset className='Fieldset'>
            <label className='Label' htmlFor='info_adicional'>
              Informação adicional
            </label>
            <textarea
              className='Input !h-[150px]'
              name='info_adicional'
              id='info_adicional'
              disabled
              defaultValue={selectedRow?.ocorrencia.info_adicional}
            />
          </fieldset>
          <fieldset className='Fieldset'>
            <label className='Label' htmlFor='estado'>
              Estado
            </label>
            <input
              type='text'
              className='Input !w-1/3'
              name='estado'
              id='estado'
              disabled
              defaultValue={selectedRow?.ocorrencia.estado}
            />
          </fieldset>
          <div className='ocorrencias-imagens'>
            {selectedRow?.images.map((key, i) => (
              <Zoom key={i}>
                <img src={`${axiosInstance.defaults.baseURL}/static/${key.nome}`} />
              </Zoom>
            ))}
          </div>
          <button onClick={closeInfoModal} className='Button red'>
            Fechar
          </button>
        </Modal>
      </section>
    </section>
  );
};

export default Ocorrencias;
