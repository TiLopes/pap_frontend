import { Controller, useForm } from 'react-hook-form';
import { useMutation, useQuery } from '@tanstack/react-query';
import { useNavigate } from 'react-router-dom';
import axiosInstance from '@helpers/axiosInstance';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';
import { useState } from 'react';
import OcorrenciaDropFiles from '@components/OcorrenciaDropFiles';
import '@styles/OcorrenciaCriar.scss';
import { format } from 'date-fns';
import { useSnackbar } from 'notistack';
import DatePicker from 'react-date-picker';
import 'react-date-picker/dist/DatePicker.css';
import 'react-calendar/dist/Calendar.css';

const Criar = () => {
  const navigate = useNavigate();
  const [acceptedFiles, setAcceptedFiles] = useState([]);
  const { enqueueSnackbar } = useSnackbar();

  async function getInfo() {
    const res = await axiosInstance.get('/api/create/ocorrencia');
    return res.data;
  }

  const { data, isSuccess } = useQuery({
    queryKey: ['condominio'],
    queryFn: getInfo,
  });

  const criarOcorrencia = useMutation({
    mutationFn: async (data) => {
      await axiosInstance.postForm('/api/create/ocorrencia', { ...data });
    },
    onSuccess: () => {
      enqueueSnackbar('Criada com sucesso', {
        variant: 'success',
      });
      navigate('..', { relative: 'path' });
    },
    onError: () => {
      enqueueSnackbar('Oops. Tente novamente', {
        variant: 'error',
      });
    },
  });

  const validationSchema = Yup.object({
    dataOcorrencia: Yup.date()
      .typeError('Data inválida')
      .max(new Date(), 'Escolha uma data até o presente')
      .required('Preencha este campo'),
    titulo: Yup.string().required('Preencha este campo'),
    descricao: Yup.string().required('Preencha este campo'),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
    control,
  } = useForm({
    mode: 'all',
    resolver: yupResolver(validationSchema),
    defaultValues: {
      dataOcorrencia: new Date(),
    },
  });

  const onSubmit = (data) => {
    criarOcorrencia.mutate({
      files: acceptedFiles,
      autor: data.autor,
      dataOcorrencia: format(data.dataOcorrencia, 'yyyy-MM-dd'),
      descricao: data.descricao,
      infoAdicional: data.infoAdicional,
      titulo: data.titulo,
    });
  };

  if (isSuccess)
    return (
      <section className='ocorrencia-criar mx-[clamp(16px,3svw,32px)] w-full'>
        <h1 className='prose my-4'>Criar ocorrência</h1>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className='input-wrapper'>
            <label>Registo por</label>
            <input
              type='text'
              readOnly
              defaultValue={data?.condomino ? data?.condomino.nome_ocupante : 'Administrador'}
              {...register('autor')}
            />
          </div>
          <div className='input-wrapper'>
            <label className='required'>Data da ocorrência</label>
            <div className='relative  w-full'>
              <Controller
                control={control}
                name='dataOcorrencia'
                render={({ field: { onChange, value } }) => (
                  <DatePicker
                    className={'w-48 rounded  border border-gray-300 p-1.5'}
                    onChange={onChange}
                    value={value}
                    locale={'pt-PT'}
                    calendarClassName={'min-w-120'}
                    clearIcon={null}
                    showLeadingZeros
                  />
                )}
              />
            </div>{' '}
            <div className='error-message'>
              {errors.dataOcorrencia?.message ? String(errors.dataOcorrencia?.message) : ''}
            </div>
          </div>
          <div className='input-wrapper'>
            <label className='required'>Título</label>
            <input type='text' {...register('titulo')} />
            <div className='error-message'>
              {errors.titulo?.message ? String(errors.titulo?.message) : ''}
            </div>
          </div>
          <div className='input-wrapper'>
            <label className='required'>Descrição</label>
            <textarea className='h-[10em] resize-none' {...register('descricao')} />
            <div className='error-message'>
              {errors.descricao?.message ? String(errors.descricao?.message) : ''}
            </div>
          </div>
          <div className='input-wrapper'>
            <label>Informação adicional</label>
            <textarea className='h-[10em] resize-none' {...register('infoAdicional')} />
          </div>
          <div className='input-wrapper'>
            <div className='container mt-5 text-center'>
              <OcorrenciaDropFiles files={(files) => setAcceptedFiles(files)} />
            </div>
          </div>
          <input type='submit' value='Criar' className='cursor-pointer !px-4 font-semibold' />
          <input
            type='button'
            onClick={() =>
              navigate('..', {
                relative: 'path',
              })
            }
            value='Cancelar'
            className='cursor-pointer !bg-red-100 !px-4 font-semibold !text-red-500'
          />
        </form>
      </section>
    );
};

export default Criar;
