import axiosInstance from '@helpers/axiosInstance';
import { useQuery } from '@tanstack/react-query';
import { useMemo } from 'react';
import Select from 'react-select';
import { useFlexLayout, usePagination, useSortBy, useTable } from 'react-table';
import '@styles/DataTable.scss';
import '@styles/DataTablePagination.scss';

const Fracoes = () => {
  const condominoFracoes = useQuery({
    queryKey: ['condomino', 'fracoes'],
    queryFn: async () => {
      const res = await axiosInstance.get('/api/my/fracoes');
      return res.data.fracoes;
    },
  });

  if (condominoFracoes.isSuccess)
    return (
      <section className='mx-[clamp(16px,3svw,32px)] w-full'>
        <h1 className='prose text-4xl mt-4 font-bold'>Minhas frações</h1>
        <div className='w-full'>
          <TabelaFracoesCondomino data={condominoFracoes.data} />
        </div>
      </section>
    );
};

const TabelaFracoesCondomino = ({ data: dataRAW }) => {
  const data = useMemo(() => dataRAW, [dataRAW]);
  const columns = useMemo(
    () => [
      {
        Header: 'Fração',
        accessor: 'id',
      },
      {
        Header: 'Permilagem escritura',
        accessor: 'permilagem_escritura',
        width: 200,
      },
      {
        Header: 'Permilagem rateio',
        accessor: 'permilagem_rateio',
        width: 200,
      },

      {
        Header: 'Tipo',
        accessor: (row) => row.tipo_tipos_fracao.descricao,
        width: 150,
      },
      {
        Header: 'Andar',
        accessor: 'andar',
        width: 100,
      },
      {
        Header: 'Data de aquisição',
        accessor: 'data_aquisicao',
        width: 175,
      },
    ],
    [],
  );

  const test = ['asd', 'ASE'];

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    selectedFlatRows,
    state: { pageIndex, pageSize, selectedRowIds, sortBy },
  } = useTable(
    {
      columns,
      data,
      initialState: {
        pageSize: 5,
      },
    },
    useFlexLayout,
    usePagination,
  );

  return (
    <div className='mt-6'>
      <div className='md-overflow-x-visible overflow-x-auto'>
        <table {...getTableProps()}>
          <thead>
            {headerGroups.map((headerGroup, i) => (
              <tr {...headerGroup.getHeaderGroupProps()} key={i}>
                {headerGroup.headers.map((column, index) => (
                  <th
                    {...column.getHeaderProps()}
                    key={index}
                    className='items-center bg-white  text-center'
                  >
                    <span className='flex h-full items-center justify-center'>
                      {column.render('Header')}
                    </span>
                  </th>
                ))}
              </tr>
            ))}
          </thead>
          <tbody {...getTableBodyProps()}>
            {page.map((row, i) => {
              prepareRow(row);
              return (
                <tr {...row.getRowProps()} key={i}>
                  {row.cells.map((cell, index) => {
                    return (
                      <td {...cell.getCellProps()} key={index} className='flex items-center'>
                        <div className='w-full'>{cell.render('Cell')}</div>
                      </td>
                    );
                  })}
                </tr>
              );
            })}
          </tbody>
        </table>
        {data.length == 0 && (
          <div className='mb-6 mt-2 w-full'>
            <h2 className='prose mx-auto text-center text-lg'>Não existem frações</h2>
          </div>
        )}
      </div>

      <div className='pagination w-full'>
        {pageCount > 1 ? (
          <div className='pagebuttons mr-6'>
            <button
              className='pageBtn !ml-0 !w-fit text-black disabled:text-gray-300'
              onClick={() => gotoPage(0)}
              disabled={!canPreviousPage}
              title='Voltar para a primeira página'
            >
              <i className='bx bx-chevron-left' />
              <i className='bx bx-chevron-left -ml-4' />
            </button>
            <button
              className='pageBtn text-black hover:cursor-pointer disabled:text-gray-300'
              onClick={() => previousPage()}
              disabled={!canPreviousPage}
            >
              <i className='bx bx-chevron-left' />
            </button>

            <button
              className='pageBtn text-black disabled:text-gray-300'
              onClick={() => nextPage()}
              disabled={!canNextPage}
            >
              <i className='bx bx-chevron-right' />
            </button>
            <button
              className='pageBtn !w-fit text-black disabled:text-gray-300'
              onClick={() => gotoPage(pageCount - 1)}
              disabled={!canNextPage}
            >
              <i className='bx bx-chevron-right' />
              <i className='bx bx-chevron-right -ml-4' />
            </button>
            <span className='ml-4 align-super'>
              Página <strong>{pageIndex + 1} </strong>
              de <strong>{pageOptions.length}</strong>
            </span>
          </div>
        ) : (
          ''
        )}
        <Select
          defaultValue={{ value: 5, label: 'Mostrar 5' }}
          onChange={(e) => {
            setPageSize(Number(e.value));
          }}
          options={[5, 10, 20].map((pageSize) => {
            return { value: pageSize, label: `Mostrar ${pageSize}` };
          })}
          className='w-48'
        />
      </div>
    </div>
  );
};

export default Fracoes;
