import axiosInstance from '@helpers/axiosInstance';
import { useAuth } from '@context/Auth.context';
import { useQuery } from '@tanstack/react-query';
import { Link, useNavigate } from 'react-router-dom';
import { useState } from 'react';
import '@styles/AdminNavbar.scss';

function toggleArrow(e) {
  const arrowParent = e.target.parentElement.parentElement; // selecting main parent of arrow
  arrowParent.classList.toggle('showMenu');
}

const Navbar = () => {
  const navigate = useNavigate();
  const [isActive, setActive] = useState(false);
  const { isLoggedIn } = useAuth();

  const logout = async () => {
    await axiosInstance.post('/api/auth/logout', {
      userType: 'condomino',
    });
    localStorage.removeItem('token');
    localStorage.removeItem('expires_at');
    localStorage.removeItem('group');
    navigate('/');
  };

  function Redirect() {
    const path = '/';
    navigate(path);
  }

  const handleToggle = () => {
    const root = document.getElementById('root');
    if (isActive) {
      root.classList.remove('sidebar-open');
      root.classList.add('sidebar-close');
    } else {
      root.classList.remove('sidebar-close');
      root.classList.add('sidebar-open');
    }

    const body = document.getElementsByTagName('body')[0];
    if (isActive) {
      body.classList.remove('sidebar-open');
      body.classList.add('sidebar-close');
    } else {
      body.classList.remove('sidebar-close');
      body.classList.add('sidebar-open');
    }

    setActive(!isActive);
  };

  const condominoInfo = useQuery({
    queryKey: ['navbar', 'condomino'],
    queryFn: async () => {
      const res = await axiosInstance.get('/api/get/me');
      return res.data.me;
    },
    onError: (err) => {
      if (err.status == 403 || err.status == 401) {
        Redirect();
      }
    },
  });

  if (isLoggedIn()) {
    return (
      <>
        <i
          className='bx bx-menu lg-!hidden my-4 ml-4 w-fit cursor-pointer text-5xl'
          onClick={handleToggle}
        ></i>
        <nav className={`sidebar ${isActive ? '' : 'close'}`}>
          <div className='logo-details'>
            <span className='logo_name'>
              {condominoInfo.isSuccess && condominoInfo.data.condominio.nome}
            </span>
            <i className='bx bx-menu cursor-pointer' id='btn' onClick={handleToggle} />
          </div>
          <ul className='nav-links'>
            <li>
              <Link to={'perfil'} onClick={isActive ? handleToggle : null}>
                <i className='bx bx-user' />
                <span className='link_name'>Perfil</span>
              </Link>
              <ul className='sub-menu blank'>
                <li>
                  <Link to={'perfil'} className='link_name'>
                    Perfil
                  </Link>
                </li>
              </ul>
            </li>
            <li>
              <div className='iocn-link'>
                <Link to='fracoes' className='link_name' onClick={isActive ? handleToggle : null}>
                  <i className='bx bx-layer' />
                  <span className='link_name'>Frações</span>
                </Link>
              </div>
              <ul className='sub-menu blank'>
                <li>
                  <Link to='fracoes' className='link_name' onClick={isActive ? handleToggle : null}>
                    Frações
                  </Link>
                </li>
              </ul>
            </li>
            <li>
              <div className='iocn-link'>
                <Link
                  to={'ocorrencias'}
                  className='link_name'
                  onClick={isActive ? handleToggle : null}
                >
                  <i className='bx bx-error-circle' />
                  <span className='link_name'>Ocorrências</span>
                </Link>
                <i className='bx bxs-chevron-down arrow' onClick={toggleArrow} />
              </div>
              <ul className='sub-menu'>
                <li>
                  <Link
                    to={'ocorrencias'}
                    className='link_name'
                    onClick={isActive ? handleToggle : null}
                  >
                    Ocorrências
                  </Link>
                </li>
                <li>
                  <Link to={'ocorrencias/criar'} onClick={isActive ? handleToggle : null}>
                    Criar ocorrência
                  </Link>
                </li>
              </ul>
            </li>
            <li>
              <div className='iocn-link'>
                <Link to={'quotas'} className='link_name'>
                  <i className='bx bx-briefcase' />
                  <span className='link_name'>Quotas</span>
                </Link>
                <i className='bx bxs-chevron-down arrow' onClick={toggleArrow} />
              </div>
              <ul className='sub-menu'>
                <li>
                  <Link to='quotas' className='link_name' onClick={isActive ? handleToggle : null}>
                    Quotas
                  </Link>
                </li>
                <li>
                  <Link
                    to='quotas/pagamentos'
                    className='link_name'
                    onClick={isActive ? handleToggle : null}
                  >
                    Pagamentos quotas
                  </Link>
                </li>
              </ul>
            </li>
            <li>
              <div className='profile-details'>
                <div className='profile-content' />
                <div className='name-job'>
                  <div className='profile_name'>
                    {condominoInfo.isSuccess && condominoInfo.data.nome_ocupante}
                  </div>
                  <div className='job'>
                    {condominoInfo.isSuccess && condominoInfo.data.username}
                  </div>
                </div>
                <i className='bx bx-log-out' onClick={logout} />
              </div>
            </li>
          </ul>
        </nav>
      </>
    );
  }
};

export default Navbar;
