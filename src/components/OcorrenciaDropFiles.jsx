import { useDropzone } from 'react-dropzone';
import { useCallback, useEffect, useState } from 'react';

/**
 * @param {Array} files Array de ficheiros
 */
function OcorrenciaDropFiles({ files }) {
  const maxSize = 1048576;
  const [fileInvalid, setFileInvalid] = useState(false);
  const [fileTooLarge, setFileTooLarge] = useState(false);

  /**
   * Adicionar ficheiro ao array de ficheiros aceites
   */
  const onDrop = useCallback((acceptedFiles) => {
    files(acceptedFiles);
    setFileInvalid(false);
    setFileTooLarge(false);
  }, []);

  /**
   * Apagar todos os ficheiros quando for cancelado
   */
  const onFileDialogCancel = useCallback(() => {
    // chamar a função que veio do componente pai
    files([]);
  }, []);

  /**
   * Criar uma zona para arrastar/inserir ficheiros que apenas aceita ficheiros do tipo:
   * - jpg, jpeg e png
   * Também é definido um limite no tamanho do ficheiro e na quantidade de ficheiros
   */
  const { isDragActive, getRootProps, getInputProps, isDragReject, acceptedFiles, fileRejections } =
    useDropzone({
      onDrop,
      onFileDialogCancel,
      accept: {
        'image/jpeg': [],
        'image/png': [],
      },
      minSize: 0,
      maxSize,
      maxFiles: 3,
      multiple: true, // aceitar vários ficheiros
    });

  // verificar se o ficheiro é demasiado grande
  const isFileTooLarge = fileRejections.length > 0 && fileRejections[0].size > maxSize;

  useEffect(() => {
    /**
     * Para todos os ficheiros rejeitados ver o tipo de erro
     * Sempre que um ficheiro é rejeitado esta função é executada
     */
    fileRejections.map(({ file, errors }) => {
      for (const error of errors) {
        switch (error.code) {
          // ficheiro inválido
          case 'file-invalid-type':
            setFileInvalid(true);
            break;
          // ficheiro demasiado grande
          case 'file-too-large':
            setFileTooLarge(true);
            break;
        }
      }
    });
  }, [fileRejections]);

  return (
    // propriedades necessárias
    <div {...getRootProps({ className: 'dropzone' })}>
      <input {...getInputProps()} />
      <div className='prose text-xl'>
        {!isDragActive && fileInvalid == false && fileTooLarge == false && acceptedFiles == 0 ? (
          <>
            <span>Imagens relevantes à ocorrência</span>
            <br />
          </>
        ) : null}
        {fileInvalid == false && fileTooLarge == false && acceptedFiles == 0 ? (
          <em className='prose text-sm'>
            (Apenas ficheiros do tipo .jpg/.jpeg e .png são válidos)
          </em>
        ) : null}
      </div>
      <div className='error-message w-full !text-center !text-base !font-medium'>
        {/* Mostrar mensagem de erro se o ficheiro for inválido */}
        {(isDragReject || fileInvalid) && <p>Tipo de ficheiro inválido!</p>}
        {/* Mostrar mensagem de erro se o ficheiro for demasiado grande */}
        {(isFileTooLarge || fileTooLarge) && <p>Ficheiro demasiado grande!</p>}
      </div>
      {/* Mostrar os ficheiros aceites */}
      <ul className='list-group mt-2'>
        {acceptedFiles.length > 0 &&
          acceptedFiles.map((acceptedFile, i) => (
            <li key={i} className='list-group-item list-group-item-success'>
              {acceptedFile.name}
            </li>
          ))}
      </ul>
    </div>
  );
}

export default OcorrenciaDropFiles;
