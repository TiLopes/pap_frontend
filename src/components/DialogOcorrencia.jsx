import * as Dialog from '@radix-ui/react-dialog';

export default function DialogOcorrencia(data) {
  return (
    <Dialog.Root>
      <Dialog.Trigger asChild>
        <button>
          <i className="bx bx-search" />
        </button>
      </Dialog.Trigger>
      <Dialog.Portal>
        <Dialog.Overlay className="DialogOverlay" />
        <Dialog.Content className="DialogContent">
          <Dialog.Title className="DialogTitle">Informações da ocorrência</Dialog.Title>
          <fieldset className="Fieldset">
            <label className="Label" htmlFor="Autor">
              Autor
            </label>
            <input
              className="Input"
              id="name"
              // defaultValue={data ? data._cells[0].data : null}
              readOnly
            />
          </fieldset>
          <fieldset className="Fieldset">
            <label className="Label" htmlFor="username">
              Username
            </label>
            <input className="Input" id="username" defaultValue="@peduarte" />
          </fieldset>
          <div
            style={{
              display: 'flex',
              marginTop: 25,
              justifyContent: 'flex-end',
            }}
          >
            <Dialog.Close asChild>
              <button className="Button red">Fechar</button>
            </Dialog.Close>
          </div>
        </Dialog.Content>
      </Dialog.Portal>
    </Dialog.Root>
  );
}
