import Modal from 'react-modal';

export default function ConfirmationModal({
  message = '',
  confirmModalIsOpen,
  onCloseModal,
  onConfirmClick,
}) {
  function onModalClose(event) {
    document.getElementById('confirmModal').classList.add('close');

    setTimeout(() => {
      onCloseModal(event);
    }, 50);
  }

  return (
    <Modal
      isOpen={confirmModalIsOpen}
      onRequestClose={onModalClose}
      className='Modal dark-theme'
      overlayClassName='Overlay'
      id='confirmModal'
    >
      <h1 className='text-center text-3xl font-bold mb-3'>Confirmação</h1>
      <h2 className='text-center'>{message}</h2>
      <div className='flex'>
        <button
          onClick={(e) => {
            onConfirmClick();
            onModalClose(e);
          }}
          className='Button green'
          type='submit'
        >
          Sim
        </button>
        <button
          onClick={(e) => {
            onModalClose(e);
          }}
          className='Button red'
          type='submit'
        >
          Não
        </button>
      </div>
    </Modal>
  );
}
