import axiosInstance from '@helpers/axiosInstance';
import '@styles/AdminNavbar.scss';
import { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { useAuth } from '../context/Auth.context';
import { useQuery } from '@tanstack/react-query';

function AdminNavbar() {
  const navigate = useNavigate();
  const [isActive, setActive] = useState(false);
  const { isLoggedIn } = useAuth();

  function Redirect() {
    const path = '/';
    navigate(path);
  }

  const logout = async () => {
    await axiosInstance.post('/api/auth/logout', {
      userType: 'condominio',
    });
    localStorage.removeItem('token');
    localStorage.removeItem('expires_at');
    localStorage.removeItem('group');
    navigate('/');
  };

  const handleToggle = () => {
    const root = document.getElementById('root');
    if (isActive) {
      root.classList.remove('sidebar-open');
      root.classList.add('sidebar-close');
    } else {
      root.classList.remove('sidebar-close');
      root.classList.add('sidebar-open');
    }

    const body = document.getElementsByTagName('body')[0];
    if (isActive) {
      body.classList.remove('sidebar-open');
      body.classList.add('sidebar-close');
    } else {
      body.classList.remove('sidebar-close');
      body.classList.add('sidebar-open');
    }

    setActive(!isActive);
  };

  const condominioInfo = useQuery({
    queryKey: ['navbar'],
    queryFn: async () => {
      const res = await axiosInstance.get('/api/condominio/info');
      return res.data.condominio;
    },
    onError: (err) => {
      if (err.response.status == 403 || err.response.status == 401) {
        Redirect();
      }
    },
  });

  function toggleArrow(e) {
    const arrowParent = e.target.parentElement.parentElement; // selecting main parent of arrow
    arrowParent.classList.toggle('showMenu');
  }

  if (isLoggedIn()) {
    return (
      <>
        <i
          className='bx bx-menu my-4 ml-4 w-fit cursor-pointer text-5xl lg:!hidden '
          onClick={handleToggle}
        ></i>
        <nav className={`sidebar ${isActive ? '' : 'close'}`}>
          <div className='logo-details'>
            <span className='logo_name'>Administração</span>
            <i className='bx bx-menu cursor-pointer' id='btn' onClick={handleToggle} />
          </div>
          <ul className='nav-links'>
            <li>
              <Link to='perfil' onClick={isActive ? handleToggle : null}>
                <i className='bx bx-user' />
                <span className='link_name'>Perfil</span>
              </Link>
              <ul className='sub-menu blank'>
                <li>
                  <a className='link_name' href='#'>
                    Perfil
                  </a>
                </li>
              </ul>
            </li>
            <li>
              <div className='iocn-link'>
                <Link to='fracoes' className='link_name' onClick={isActive ? handleToggle : null}>
                  <i className='bx bx-layer' />
                  <span className='link_name'>Frações</span>
                </Link>
                <i className='bx bxs-chevron-down arrow' onClick={toggleArrow} />
              </div>
              <ul className='sub-menu'>
                <li>
                  <Link to='fracoes' className='link_name' onClick={isActive ? handleToggle : null}>
                    Frações
                  </Link>
                </li>
                <li>
                  <Link to='fracoes/criar' onClick={isActive ? handleToggle : null}>
                    Criar fração
                  </Link>
                </li>
              </ul>
            </li>
            <li>
              <div className='iocn-link'>
                <Link to='condomino' className='link_name' onClick={isActive ? handleToggle : null}>
                  <i className='bx bx-group' />
                  <span className='link_name'>Condóminos</span>
                </Link>
                <i className='bx bxs-chevron-down arrow' onClick={toggleArrow} />
              </div>
              <ul className='sub-menu'>
                <li>
                  <Link
                    to='condomino'
                    className='link_name'
                    onClick={isActive ? handleToggle : null}
                  >
                    Condóminos
                  </Link>
                </li>
                <li>
                  <Link to='condomino/criar' onClick={isActive ? handleToggle : null}>
                    Criar condómino
                  </Link>
                </li>
              </ul>
            </li>
            <li>
              <div className='iocn-link'>
                <a className='link_name'>
                  <i className='bx bx-cog' />
                  <span className='link_name'>Manutenção</span>
                </a>
                <i className='bx bxs-chevron-down arrow' onClick={toggleArrow} />
              </div>
              <ul className='sub-menu'>
                <li>
                  <a className='link_name'>Manutenção</a>
                </li>
                <li>
                  <Link to='ocorrencias' onClick={isActive ? handleToggle : null}>
                    Ocorrências
                  </Link>
                </li>
              </ul>
            </li>
            <li>
              <div className='iocn-link'>
                <a className='link_name'>
                  <i className='bx bx-briefcase' />
                  <span className='link_name'>Gestão</span>
                </a>
                <i className='bx bxs-chevron-down arrow' onClick={toggleArrow} />
              </div>
              <ul className='sub-menu'>
                <li>
                  <a className='link_name'>Gestão</a>
                </li>
                <li>
                  <Link to='quotas' onClick={isActive ? handleToggle : null}>
                    Quotas
                  </Link>
                </li>
                <li>
                  <Link to='movimentos' onClick={isActive ? handleToggle : null}>
                    Movimentos do condomínio
                  </Link>
                </li>
              </ul>
            </li>
            <li>
              <div className='profile-details'>
                <div className='profile-content' />
                <div className='name-job'>
                  <div className='profile_name'>
                    {condominioInfo.isSuccess && condominioInfo.data.nome}
                  </div>
                  <div className='job text-ellipsis'>
                    {condominioInfo.isSuccess && condominioInfo.data.morada} &#183;{' '}
                    {condominioInfo.isSuccess && condominioInfo.data.cod_postal}
                  </div>
                </div>
                <i className='bx bx-log-out' onClick={logout} />
              </div>
            </li>
          </ul>
        </nav>
      </>
    );
  }
}

export default AdminNavbar;
