import React, { useState, useEffect } from 'react';
import '@styles/DataTablePagination.scss';

export default function DataTablePagination({ pageChangeHandler, totalRows, rowsPerPage }) {
  const noOfPages = Math.ceil(totalRows / rowsPerPage);

  const [currentPage, setCurrentPage] = useState(0);

  // console.log(noOfPages);

  const [canGoBack, setCanGoBack] = useState(false);
  const [canGoNext, setCanGoNext] = useState(true);

  // Onclick handlers for the butons
  const onNextPage = () => setCurrentPage(currentPage + 1);
  const onPrevPage = () => setCurrentPage(currentPage - 1);
  const onPageSelect = (pageNo) => setCurrentPage(pageNo);

  useEffect(() => {
    if (noOfPages === currentPage + 1) {
      setCanGoNext(false);
    } else {
      setCanGoNext(true);
    }
    if (currentPage === 0) {
      setCanGoBack(false);
    } else {
      setCanGoBack(true);
    }
  }, [noOfPages, currentPage]);

  // To set the starting index of the page
  useEffect(() => {
    // const skipFactor = currentPage - 1 * rowsPerPage;
    pageChangeHandler(currentPage);
  }, [currentPage]);

  return (
    <>
      {noOfPages > 1 ? (
        <div className='pagination'>
          <div className='pagebuttons'>
            <button
              className='pageBtn !ml-0 !w-fit text-black hover:cursor-pointer disabled:text-gray-300'
              onClick={() => setCurrentPage(0)}
              disabled={!canGoBack}
              title='Voltar para a primeira página'
            >
              <i className='bx bx-chevron-left' />
              <i className='bx bx-chevron-left -ml-4' />
            </button>
            <button
              className='pageBtn text-black hover:cursor-pointer disabled:text-gray-300'
              onClick={onPrevPage}
              disabled={!canGoBack}
            >
              <i className='bx bx-chevron-left' />
            </button>

            <button
              className='pageBtn text-black hover:cursor-pointer disabled:text-gray-300'
              onClick={onNextPage}
              disabled={!canGoNext}
            >
              <i className='bx bx-chevron-right' />
            </button>
            <button
              className='pageBtn !w-fit text-black hover:cursor-pointer disabled:text-gray-300'
              onClick={() => setCurrentPage(noOfPages - 1)}
              disabled={!canGoNext}
            >
              <i className='bx bx-chevron-right' />
              <i className='bx bx-chevron-right -ml-4' />
            </button>
            <span className='ml-4 align-super'>
              Página <strong>{currentPage + 1} </strong>
              de <strong>{noOfPages}</strong>
            </span>
          </div>
        </div>
      ) : (
        ''
      )}
    </>
  );
}
