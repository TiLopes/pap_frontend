import { useMemo } from 'react';
import { useFlexLayout, useTable } from 'react-table';
import '@styles/DataTable.scss';

/**
 * Componente que renderiza uma tabela com paginação manual.
 * @param {Object} props - Propriedades do componente.
 * @param {Array.<Object>} props.columns - As colunas da tabela.
 * @param {Array.<Object>} props.data - A informação a ser exibida na tabela.
 * @param {boolean} props.manualPagination - Indica se a paginação é manual.
 * @param {boolean} props.isLoading - Indica se a informação está sendo carregada.
 * @returns {JSX.Element} O componente da tabela.
 */
export default function DataTable({ columns, data, isLoading, manualPagination = false }) {
  const columnData = useMemo(() => columns, [columns]); // Colunas da tabela
  const rowData = useMemo(() => data, [data]); // Informação

  // Criar uma tabela com paginação manual usando o hook useTable
  const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } = useTable(
    {
      columns: columnData,
      data: rowData,
      manualPagination,
      initialState: {
        hiddenColumns: ['id', 'id_pagamento'],
      },
    },
    useFlexLayout,
  );

  // Se ainda não houver informação, mostra um spinner de carregamento
  if (isLoading) {
    return (
      <>
        <table {...getTableProps()} className='ml-px mt-px'>
          <thead>
            {headerGroups.map((headerGroup, i) => (
              <tr {...headerGroup.getHeaderGroupProps()} key={i}>
                {headerGroup.headers.map((column, index) => (
                  <th
                    {...column.getHeaderProps()}
                    key={index}
                    className='items-center bg-white text-center'
                  >
                    <span className='flex h-full items-center justify-center'>
                      {column.render('Header')}
                    </span>
                  </th>
                ))}
              </tr>
            ))}
          </thead>
        </table>
        <div className='relative flex h-36 w-full items-center justify-center'>
          <div className='loading-spinner'></div>
        </div>
      </>
    );
  }

  // Renderiza a tabela com as colunas e a informação
  return (
    <table {...getTableProps()} className='ml-px mt-px'>
      <thead>
        {headerGroups.map((headerGroup, i) => (
          <tr {...headerGroup.getHeaderGroupProps()} key={i}>
            {headerGroup.headers.map((column, index) => (
              <th
                {...column.getHeaderProps()}
                key={index}
                className='items-center bg-white  text-center'
              >
                <span className='flex h-full items-center justify-center'>
                  {column.render('Header')}
                </span>
              </th>
            ))}
          </tr>
        ))}
      </thead>
      <tbody {...getTableBodyProps()}>
        {rows.map((row, i) => {
          prepareRow(row);
          return (
            <tr {...row.getRowProps()} key={i}>
              {row.cells.map((cell, index) => (
                <td {...cell.getCellProps()} key={index} className='flex items-center'>
                  <div className='w-full'>{cell.render('Cell')}</div>
                </td>
              ))}
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}
