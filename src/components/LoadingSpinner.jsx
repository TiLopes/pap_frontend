import '@styles/LoadingSpinner.scss';

function LoadingSpinner() {
  return (
    <div className='spinner-overlay flex items-center justify-center'>
      <div className='loading-spinner'></div>
    </div>
  );
}

export default LoadingSpinner;
