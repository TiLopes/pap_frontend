import axiosInstance from '@helpers/axiosInstance';
import { useEffect, useState } from 'react';
import { useQuery } from '@tanstack/react-query';
import { useNavigate } from 'react-router-dom';
import DataTable from './DataTable';
import DataTablePagination from './DataTablePagination';
import Modal from 'react-modal';
import Select from 'react-select';
import { useForm, Controller } from 'react-hook-form';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

Modal.setAppElement('#home-section');

function VerFracoes() {
  let condominosOptions = [];
  const [estadoSelect, setEstadoSelect] = useState();
  const [selectedInfoRow, setSelectedInfoRow] = useState();
  const [selectedEditRow, setSelectedEditRow] = useState();
  const [infoModalClose, setInfoModalClose] = useState(false);
  const [infoIsOpen, setInfoIsOpen] = useState(false);
  const [editModalClose, setEditModalClose] = useState(false);
  const [editIsOpen, setEditIsOpen] = useState(false);
  const [pageData, setPageData] = useState({
    rowData: [],
    isLoading: false,
    totalPages: 0,
    totalOcorrencias: 0,
  });
  const [refreshData, setRefreshData] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);
  const columns = [
    {
      Header: 'Fração',
      accessor: 'id_fracao',
      width: 100,
    },
    {
      Header: 'Ocupante',
      accessor: 'id_condomino_condomino.nome_ocupante',
      maxWidth: 200,
    },
    {
      Header: 'Permilagem escritura',
      accessor: 'permilagem_escritura',
      width: 200,
    },
    {
      Header: 'Permilagem rateio',
      accessor: 'permilagem_rateio',
      width: 200,
    },
    {
      Header: 'Tipo',
      accessor: 'tipo',
      width: 150,
    },
    {
      Header: 'Andar',
      accessor: 'andar',
      width: 100,
    },
    {
      Header: 'Estado',
      accessor: 'estado',
      width: 100,
    },
    {
      Header: 'Ações',
      accessor: 'acoes',
      Cell: (props) => (
        <div className='flex justify-evenly items-center'>
          <button
            className='items-center flex'
            onClick={async function consultar() {
              try {
                const res = await axiosInstance.get(
                  `/api/get/fracao/${props?.row.original?.id_fracao}`,
                );
                setSelectedInfoRow(res.data);
                openInfoModal();
              } catch (err) {
                console.error(err);
                if (err.response.status === 403 || err.response.status === 401) {
                  Redirect();
                }
              }
            }}
          >
            <i className='bx bx-search text-xl text-sky-400' title='Ver' />
          </button>
          <button
            className='items-center flex'
            onClick={async function consultar() {
              try {
                const fracao = await axiosInstance
                  .get(`/api/get/fracao/${props?.row?.original.id_fracao}`)
                  .then((res) => res.data);

                // * hack para definir valor do select caso este não seja alterado
                setValue('estado', {
                  value: fracao?.fracao.estado,
                  label: fracao?.fracao.estado,
                });
                setValue('tipo_fracao', {
                  value: fracao?.fracao.tipo,
                  label: fracao?.fracao.tipo,
                });
                setValue('ocupante', {
                  value: fracao?.fracao.id_condomino_condomino?.id,
                  label: fracao?.fracao.id_condomino_condomino?.nome_ocupante,
                });

                setSelectedEditRow({ fracaoInfo: fracao?.fracao });
                openEditModal();
              } catch (err) {
                console.error(err);
                if (err.response.status === 403 || err.response.status === 401) {
                  Redirect();
                }
              }
            }}
          >
            <i className='bx bxs-edit text-xl text-green-400' title='Editar'></i>
          </button>
        </div>
      ),
    },
  ];
  const navigate = useNavigate();

  const { isError, isSuccess, data } = useQuery({
    queryKey: ['vpermFracoes'],
    queryFn: verifyPermission,
  });

  const optionsTipoFracao = [
    { value: 'Administração', label: 'Administração' },
    { value: 'Apartamento', label: 'Apartamento' },
    { value: 'Arrumos', label: 'Arrumos' },
    { value: 'Atelier', label: 'Atelier' },
    { value: 'Auditório', label: 'Auditório' },
    { value: 'Cave', label: 'Cave' },
    { value: 'Escritório', label: 'Escritório' },
    { value: 'Garagem', label: 'Garagem' },
    { value: 'Loja', label: 'Loja' },
    { value: 'Restaurante/Cafetaria', label: 'Restaurante/Cafetaria' },
    { value: 'Segurança', label: 'Segurança' },
    { value: 'Sótão', label: 'Sótão' },
  ];

  async function verifyPermission() {
    const res = await axiosInstance.get('/api/get/condominos');
    return res.data;
  }

  const getData = async (pageNo = 0) => {
    const response = await axiosInstance(
      `/api/get/fracoes?limit=10&offset=${pageNo * 10}&order=id&dir=ASC`,
    );

    return response;
  };

  // quando mudar de página "buscar" as próximas 10 ocorrências
  useEffect(() => {
    setPageData((prevState) => ({
      ...prevState,
      rowData: [],
      isLoading: true,
    }));

    getData(currentPage)
      .then((info) => {
        const { data } = info;
        const totalFracoes = data.count;
        const totalPages = Math.floor(totalFracoes / 10);
        setTimeout(() => {
          setPageData({
            isLoading: false,
            rowData: data.fracoes,
            totalPages,
            totalFracoes,
          });
        }, 200);
      })
      .catch((err) => {
        if (err.response.status === 403 || err.response.status === 401) {
          Redirect();
        }
      });
  }, [currentPage]);

  useEffect(() => {
    setPageData((prevState) => ({
      ...prevState,
      rowData: [],
      isLoading: true,
    }));

    getData(currentPage).then((info) => {
      const { data } = info;
      const totalFracoes = data.count;
      const totalPages = Math.floor(totalFracoes / 10);

      setPageData({
        isLoading: false,
        rowData: data.fracoes,
        totalPages,
        totalFracoes,
      });
    });

    setRefreshData(false);
  }, [refreshData]);

  function openInfoModal() {
    setInfoModalClose(false);
    setInfoIsOpen(true);
  }

  function closeInfoModal() {
    setInfoModalClose(true);
    setTimeout(() => {
      setInfoIsOpen(false);
    }, 50);
  }

  function openEditModal() {
    console.log(selectedEditRow);
    setEditModalClose(false);
    setEditIsOpen(true);
  }

  function closeEditModal() {
    reset();
    setEstadoSelect(null);
    setEditModalClose(true);
    setTimeout(() => {
      setEditIsOpen(false);
    }, 50);
  }

  function Redirect() {
    const path = '/login/condominio';
    navigate(path);
  }

  const {
    register: registerEditar,
    handleSubmit: handleSubmitEditar,
    formState: { errors: errorsEditar },
    setValue,
    control,
    reset,
  } = useForm();

  function onSubmitEditar(form) {
    console.log(form);
    try {
      toast.promise(
        async () => {
          await axiosInstance.post(`/api/edit/fracao/${form.id_fracao}`, {
            andar: form.andar,
            escritura: Number(form.escritura),
            estado: form.estado.value,
            tipo: form.tipo_fracao.value,
            ocupante: form.ocupante?.value,
          });
        },
        {
          error: 'Erro ao editar fração',
          success: 'Fração editada com sucesso',
        },
      );

      setRefreshData(true);
      closeEditModal();
    } catch (e) {
      console.error({ e });
    }
  }

  if (isError) {
    Redirect();
    return;
  }

  if (isSuccess) {
    for (const condomino of data) {
      condominosOptions = [
        ...condominosOptions,
        { value: condomino.id, label: condomino.nome_ocupante },
      ];
    }

    return (
      <section className='ver-fracoes mt-12'>
        <div className='min-h-[460px] max-h-fit'>
          <DataTable columns={columns} data={pageData.rowData} isLoading={pageData.isLoading} />
        </div>
        <DataTablePagination
          totalRows={pageData.totalOcorrencias}
          pageChangeHandler={setCurrentPage}
          rowsPerPage={10}
        />
        <Modal
          isOpen={infoIsOpen}
          onRequestClose={closeInfoModal}
          className={`Modal ${infoModalClose ? 'close' : ''} dark-theme`}
          overlayClassName='Overlay'
          id='modal'
        >
          <fieldset className='Fieldset'>
            <label className='Label' htmlFor='id'>
              Fração
            </label>
            <input
              type='text'
              className='Input'
              name='id'
              disabled
              defaultValue={selectedInfoRow?.fracao.id}
            />
          </fieldset>
          <fieldset className='Fieldset'>
            <label className='Label' htmlFor='tipo'>
              Tipo da fração
            </label>
            <input
              type='text'
              className='Input'
              name='tipo'
              disabled
              defaultValue={selectedInfoRow?.fracao.tipo}
            />
          </fieldset>
          <fieldset className='Fieldset'>
            <label className='Label' htmlFor='andar'>
              Andar
            </label>
            <input
              type={'text'}
              className='Input'
              name='andar'
              disabled
              defaultValue={selectedInfoRow?.fracao.andar}
            />
          </fieldset>
          <fieldset className='Fieldset'>
            <label className='Label' htmlFor='escritura'>
              Escritura
            </label>
            <input
              type={'text'}
              className='Input'
              name='escritura'
              disabled
              defaultValue={selectedInfoRow?.fracao.permilagem_escritura}
            />
          </fieldset>
          <fieldset className='Fieldset'>
            <label className='Label' htmlFor='estado'>
              Estado
            </label>
            <input
              type={'text'}
              className='Input'
              name='estado'
              disabled
              defaultValue={selectedInfoRow?.fracao.estado}
            />
          </fieldset>
          <fieldset className='Fieldset'>
            <label className='Label' htmlFor='ocupante'>
              Ocupante
            </label>
            <input
              type={'text'}
              className='Input'
              name='ocupante'
              disabled
              defaultValue={selectedInfoRow?.fracao.id_condomino_condomino?.nome_ocupante}
            />
          </fieldset>
          <button onClick={closeInfoModal} className='Button red'>
            Fechar
          </button>
        </Modal>
        <Modal
          isOpen={editIsOpen}
          onRequestClose={closeEditModal}
          className={`Modal ${editModalClose ? 'close' : ''} dark-theme`}
          overlayClassName='Overlay'
          id='modal'
        >
          <form onSubmit={handleSubmitEditar(onSubmitEditar)}>
            <fieldset className='Fieldset'>
              <label className='Label' htmlFor='id_fracao'>
                Fração
              </label>
              <input
                type='text'
                className='Input'
                name='id_fracao'
                readOnly
                defaultValue={selectedEditRow?.fracaoInfo.id}
                {...registerEditar('id_fracao')}
              />
            </fieldset>
            <fieldset className='Fieldset'>
              <label className='Label' htmlFor='tipo'>
                Tipo da fração
              </label>
              <Select
                options={optionsTipoFracao}
                inputId='tipo_fracao'
                className='w-full'
                placeholder='Indique o tipo da fração'
                defaultValue={{
                  value: selectedEditRow?.fracaoInfo.tipo,
                  label: selectedEditRow?.fracaoInfo.tipo,
                }}
                {...registerEditar('tipo_fracao')}
                onChange={(selected) => setValue('tipo_fracao', selected)}
              />
            </fieldset>
            <fieldset className='Fieldset'>
              <label className='Label' htmlFor='andar'>
                Andar
              </label>
              <input
                type={'text'}
                className='Input'
                name='andar'
                defaultValue={selectedEditRow?.fracaoInfo.andar}
                {...registerEditar('andar')}
              />
            </fieldset>
            <fieldset className='Fieldset'>
              <label className='Label' htmlFor='escritura'>
                Escritura
              </label>
              <input
                type={'text'}
                className='Input'
                name='escritura'
                defaultValue={selectedEditRow?.fracaoInfo.escritura}
                {...registerEditar('escritura')}
              />
            </fieldset>
            <fieldset className='Fieldset'>
              <label className='Label' htmlFor='estado'>
                Estado
              </label>
              <Select
                inputId='estado'
                options={[
                  { value: 'Livre', label: 'Livre' },
                  { value: 'Ocupado', label: 'Ocupado' },
                ]}
                defaultValue={{
                  value: selectedEditRow?.fracaoInfo.estado,
                  label: selectedEditRow?.fracaoInfo.estado,
                }}
                onChange={(selected) => setValue('estado', selected)}
                name='estado'
                value={
                  estadoSelect || {
                    value: selectedEditRow?.fracaoInfo.estado,
                    label: selectedEditRow?.fracaoInfo.estado,
                  }
                }
                {...registerEditar('estado')}
              />
            </fieldset>
            <fieldset className='Fieldset'>
              <label className='Label' htmlFor='ocupante'>
                Ocupante
              </label>
              <Select
                inputId='ocupante'
                options={condominosOptions}
                name='ocupante'
                isClearable={true}
                defaultValue={{
                  value: selectedEditRow?.fracaoInfo.id_condomino_condomino?.nome_ocupante,
                  label: selectedEditRow?.fracaoInfo.id_condomino_condomino?.nome_ocupante,
                }}
                {...registerEditar('ocupante')}
                onChange={(selected) => {
                  if (!selected) {
                    setValue('estado', { value: 'Livre', label: 'Livre' });
                    setValue('ocupante', selected);
                    setEstadoSelect({ value: 'Livre', label: 'Livre' });
                  } else {
                    setValue('ocupante', selected);
                    setEstadoSelect({ value: 'Ocupado', label: 'Ocupado' });
                    setValue('estado', { value: 'Ocupado', label: 'Ocupado' });
                  }
                }}
              />
            </fieldset>
            <div className='flex'>
              <button type='submit' className='Button green'>
                Guardar
              </button>
              <button onClick={closeEditModal} className='Button red' type='button'>
                Fechar
              </button>
            </div>
          </form>
        </Modal>
        <ToastContainer
          position='top-right'
          autoClose={2500}
          hideProgressBar={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss={false}
          draggable={false}
          pauseOnHover
          theme='colored'
        />
      </section>
    );
  }
}

export default VerFracoes;
